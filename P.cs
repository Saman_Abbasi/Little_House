﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Little_House__
{
    public partial class PersonX : Form
    {
        public PersonX()
        {
            InitializeComponent();
        }
        private void label7_Click(object sender, EventArgs e)
        {

        }
        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }
        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }
        private void KoliBox_Enter(object sender, EventArgs e)
        {

        }
        private void textBox15_TextChanged(object sender, EventArgs e)
        {

        }
        private void label22_Click(object sender, EventArgs e)
        {

        }
        private void label20_Click(object sender, EventArgs e)
        {

        }
        private void DaneshAmozBox_Enter(object sender, EventArgs e)
        {

        }
        private void PersonX_Load(object sender, EventArgs e)
        {
            PersianCalendar persian = new PersianCalendar();
            comboBox6.SelectedText = persian.GetDayOfMonth(DateTime.Now).ToString("00");
            comboBox8.SelectedText = persian.GetYear(DateTime.Now).ToString("0000");
            comboBox7.SelectedIndex = persian.GetMonth(DateTime.Now) - 1;
            //comboBox6.SelectedIndex = 0;
            //comboBox7.SelectedIndex = 0;
            //comboBox8.SelectedIndex = 15;

            //=====================================================//
            for (int i = 1310; i <= 1420; i++)
            {
                comboBox8.Items.Add(i);
                comboBox12.Items.Add(i);
            }

            //=====================================================//


            LittleEntity PersonCombo = new LittleEntity();
            comboBox3.DataSource = PersonCombo.BNoekomaks.ToList();
            comboBox3.DisplayMember = "title";
            comboBox3.ValueMember = "id";

            comboBox4.DataSource = PersonCombo.BNoeposheshes.ToList();
            comboBox4.DisplayMember = "title";
            comboBox4.ValueMember = "id";

            comboBox5.DataSource = PersonCombo.BTahols.ToList();
            comboBox5.DisplayMember = "title";
            comboBox5.ValueMember = "id";

            comboBox1.DataSource = PersonCombo.BVaziyats.ToList();
            comboBox1.DisplayMember = "Title";
            comboBox1.ValueMember = "id";

            //====================================================================//
            //LittleEntity Queryy = new LittleEntity();
            //var afradlinqq = Queryy.People.ToList();
            //var f = from y in afradlinqq
            //        select new
            //        {
            //            Name = y.name + " " + y.lname,
            //            y.nationalcode,
            //            bnoekomak = y.BNoekomak.title,
            //            y.id,
            //            y.shenasname,
            //            y.bdate,
            //            y.faname,
            //            y.vasile,
            //            y.child,
            //            y.sarparast,
            //            y.tahsilat,
            //            y.address,
            //            y.tell,
            //            y.shomarehesab,
            //            y.uni,
            //            y.reshte,
            //            bnoeposhesh = y.BNoeposhesh.title,
            //            y.datevorod,
            //            y.maghta,
            //            y.shahr,
            //            y.madrese,
            //            y.noemaloliyat,
            //            y.tozihat,
            //            y.BVaziyat.title
            //        };//,tahol=y.BTahol.title};
            //dataGridView1afrad.DataSource = f.ToList();


            //====================================================================//
            //*******//حذف تو دیتا بیس جدول افراد حامی ندارم 

            ////////////comboBox2.DataSource = PersonCombo.Hamis.Select(x => new { Name = x.name + " " + x.lname }).ToList();
            ////////////comboBox2.DisplayMember = "name";
            ///****////comboBox2.ValueMember = "id";

        }
        //==============================================================================//
        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            PersianCalendar persian = new PersianCalendar();
            switch (comboBox1.SelectedIndex)
            {

                case 1: // Danesh Jooo
                    //===================  malol   =======================//
                    textBox25.Visible = false;
                    textBox23.Visible = false;
                    label28.Visible = false;
                    label29.Visible = false;
                    //=====================================================//
                    //===================  danesh amooz  ==================//
                    textBox20.Visible = false;
                    textBox21.Visible = false;
                    textBox22.Visible = false;
                    label24.Visible = false;
                    label25.Visible = false;
                    label26.Visible = false;
                    //=====================================================//
                    //==================   danesh joo =====================//
                    //comboBox10.Visible = true;
                    comboBox11.Visible = true;
                    comboBox12.Visible = true;
                    label19.Visible = true;
                    label20.Visible = true;
                    label21.Visible = true;
                    label22.Visible = true;
                    label27.Visible = true;
                    textBox7.Visible = true;
                    textBox17.Visible = true;
                    textBox16.Visible = true;
                    textBox19.Visible = true;
                    //comboBox10.SelectedText = persian.GetDayOfMonth(DateTime.Now).ToString();
                    comboBox12.SelectedText = persian.GetYear(DateTime.Now).ToString();
                    comboBox11.SelectedIndex = persian.GetMonth(DateTime.Now) - 1;
                    //======================================================//
                    break;
                case 2:
                    //==================  malol  ============================//
                    textBox25.Visible = false;
                    textBox23.Visible = false;
                    label28.Visible = false;
                    label29.Visible = false;
                    //=====================================================//
                    //==================   danesh amooz ===================//
                    textBox20.Visible = true;
                    textBox21.Visible = true;
                    textBox22.Visible = true;
                    label24.Visible = true;
                    label25.Visible = true;
                    label26.Visible = true;
                    //=====================================================//
                    //=================  danesh jooo ======================//
                    label19.Visible = false;
                    label20.Visible = false;
                    label21.Visible = false;
                    label22.Visible = false;
                    label27.Visible = false;
                    textBox7.Visible = false;
                    textBox17.Visible = false;
                    textBox16.Visible = false;
                    textBox19.Visible = false;
                    //comboBox10.Visible = false;
                    comboBox11.Visible = false;
                    comboBox12.Visible = false;
                    //=====================================================//
                    break;
                case 3:
                    //===================  malol  ======================//
                    textBox25.Visible = true;
                    textBox23.Visible = true;
                    label28.Visible = true;
                    label29.Visible = true;
                    //=====================================================//
                    //=================   danesh amooz ====================//
                    textBox20.Visible = false;
                    textBox21.Visible = false;
                    textBox22.Visible = false;
                    label24.Visible = false;
                    label25.Visible = false;
                    label26.Visible = false;
                    //=====================================================//
                    //====================   danesh jo   ==================//
                    label19.Visible = false;
                    label20.Visible = false;
                    label21.Visible = false;
                    label22.Visible = false;
                    label27.Visible = false;
                    textBox7.Visible = false;
                    textBox17.Visible = false;
                    textBox16.Visible = false;
                    textBox19.Visible = false;
                    //comboBox10.Visible = false;
                    comboBox11.Visible = false;
                    comboBox12.Visible = false;
                    break;

                case 0:
                case 5:
                case 4:
                    //=================  malol  ========================//
                    textBox25.Visible = false;
                    textBox23.Visible = false;
                    label28.Visible = false;
                    label29.Visible = false;
                    //=====================================================//
                    //=====================  danesh amooz =================//
                    textBox20.Visible = false;
                    textBox21.Visible = false;
                    textBox22.Visible = false;
                    label24.Visible = false;
                    label25.Visible = false;
                    label26.Visible = false;
                    //=====================================================//
                    //===================  daneshjoo  ========================//
                    label19.Visible = false;
                    label20.Visible = false;
                    label21.Visible = false;
                    label22.Visible = false;
                    label27.Visible = false;
                    textBox7.Visible = false;
                    textBox17.Visible = false;
                    textBox16.Visible = false;
                    textBox19.Visible = false;
                    //comboBox10.Visible = false;
                    comboBox11.Visible = false;
                    comboBox12.Visible = false;
                    break;

            }
        }
        //==============================================================================//
        private void label23_Click(object sender, EventArgs e)
        {

        }
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void comboBox1_SelectionChangeCommitted(object sender, EventArgs e)
        {

        }
        private void button1_Click_1(object sender, EventArgs e)
        {

            LittleEntity context1 = new LittleEntity();
            Person PersonAdd = new Person();
            switch (comboBox1.SelectedIndex)
            {
                case 1: // Danesh Jooo
                    //=================================  Koli ===================================//
                    PersonAdd.name = textBox1.Text;
                    PersonAdd.lname = textBox2.Text;
                    PersonAdd.faname = textBox3.Text;
                    PersonAdd.shenasname = textBox4.Text;
                    PersonAdd.nationalcode = textBox5.Text;
                    PersonAdd.sarparast = textBox9.Text;
                    PersonAdd.tahsilat = textBox10.Text;
                    PersonAdd.tell = textBox11.Text;
                    PersonAdd.shomarehesab = textBox12.Text;
                    //------------------------- B Date  ---------------------------------//
                    string da1 = (comboBox6.SelectedIndex + 1).ToString("00");//
                    string mon1 = (comboBox7.SelectedIndex + 1).ToString("00");//
                    string yea1 = (comboBox8.SelectedIndex + 1310).ToString("0000");

                    try
                    {
                        string dat1 = yea1 + "/" + mon1 + "/" + da1;
                        PersonAdd.bdate = Convert.ToDateTime(dat1);
                    }
                    catch (Exception ex)
                    {
                        string datealaki = "1400/12/31";
                        PersonAdd.bdate = Convert.ToDateTime(datealaki);
                    }
                    //-----------------------------------------------------------------//

                    //------------------------- Child ---------------------------------//
                    if (textBox13.Text == "" || textBox11.Text == null)
                    {
                        PersonAdd.child = (Byte)0;
                    }
                    else
                    {
                        PersonAdd.child = Convert.ToByte(textBox13.Text);
                    }
                    //------------------------- ComboS --------------------------//
                    PersonAdd.vaziyat = Convert.ToByte(comboBox1.SelectedValue);
                    PersonAdd.noekomak_id = Convert.ToByte(comboBox3.SelectedValue);
                    PersonAdd.noeposhesh_id = Convert.ToByte(comboBox4.SelectedValue);
                    PersonAdd.tahol_id = Convert.ToByte(comboBox5.SelectedValue);
                    //-------------------------------------------------------------//
                    PersonAdd.address = textBox14.Text;
                    PersonAdd.tozihat = textBox15.Text;
                    //^^^^^^^^^^^^^^^^^^^^^^ Koli ===================================//
                    //------------------------- V Date  ---------------------------------//

                    string vmon = (comboBox11.SelectedIndex + 1).ToString("00");//
                    string vyea = (comboBox12.SelectedIndex + 1310).ToString("0000");

                    try
                    {
                        string vdat = vyea + "/" + vmon + "/" + "01";
                        PersonAdd.datevorod = Convert.ToDateTime(vdat);
                    }
                    catch (Exception ex)
                    {
                        string vdat = "1410/12/31";
                        PersonAdd.datevorod = Convert.ToDateTime(vdat);
                    }
                    //-----------------------------------------------------------------//
                    PersonAdd.uni = textBox16.Text;
                    PersonAdd.reshte = textBox7.Text;
                    PersonAdd.shahr = textBox17.Text;
                    PersonAdd.maghta = textBox19.Text;
                    //================================= Save ====================================//
                    try
                    {

                        context1.People.Add(PersonAdd);
                        context1.SaveChanges();
                        this.Close();
                        MessageBox.Show("دانشجو جدید افروزده شد");

                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message, " Khata !!!! ");
                    }
                    break;
                case 2: // Danesh AmooZz
                    //=================================  Koli ===================================//
                    PersonAdd.name = textBox1.Text;
                    PersonAdd.lname = textBox2.Text;
                    PersonAdd.faname = textBox3.Text;
                    PersonAdd.shenasname = textBox4.Text;
                    PersonAdd.nationalcode = textBox5.Text;
                    PersonAdd.sarparast = textBox9.Text;
                    PersonAdd.tahsilat = textBox10.Text;
                    PersonAdd.tell = textBox11.Text;
                    PersonAdd.shomarehesab = textBox12.Text;
                    //------------------------- B Date  ---------------------------------//
                    string da2 = (comboBox6.SelectedIndex + 1).ToString("00");//
                    string mon2 = (comboBox7.SelectedIndex + 1).ToString("00");//
                    string yea2 = (comboBox8.SelectedIndex + 1310).ToString("0000");

                    try
                    {
                        string dat2 = yea2 + "/" + mon2 + "/" + da2;
                        PersonAdd.bdate = Convert.ToDateTime(dat2);
                    }
                    catch (Exception ex)
                    {
                        string datealaki1 = "1400/12/31";
                        PersonAdd.bdate = Convert.ToDateTime(datealaki1);
                    }
                    //-----------------------------------------------------------------//
                    //------------------------- Child ---------------------------------//
                    if (textBox13.Text == "" || textBox11.Text == null)
                    {
                        PersonAdd.child = 0;
                    }
                    else
                    {
                        PersonAdd.child = Convert.ToByte(textBox13.Text);
                    }
                    //------------------------- ComboS --------------------------//
                    PersonAdd.vaziyat = Convert.ToByte(comboBox1.SelectedValue);
                    PersonAdd.noekomak_id = Convert.ToByte(comboBox3.SelectedValue);
                    PersonAdd.noeposhesh_id = Convert.ToByte(comboBox4.SelectedValue);
                    PersonAdd.tahol_id = Convert.ToByte(comboBox5.SelectedValue);
                    //-------------------------------------------------------------//
                    PersonAdd.address = textBox14.Text;
                    PersonAdd.tozihat = textBox15.Text;
                    //^^^^^^^^^^^^^^^^^^^^^^ Koli ===================================//
                    PersonAdd.shahr = textBox19.Text;
                    PersonAdd.maghta = textBox20.Text;
                    PersonAdd.madrese = textBox21.Text;
                    //================================= Save ====================================//
                    try
                    {

                        context1.People.Add(PersonAdd);
                        context1.SaveChanges();
                        this.Close();
                        MessageBox.Show("دانش آموز جدید افزوده شد");

                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message, " Khata !!!! ");
                    }
                    break;
                case 3: // MalooL
                    //=================================  Koli ===================================//
                    PersonAdd.name = textBox1.Text;
                    PersonAdd.lname = textBox2.Text;
                    PersonAdd.faname = textBox3.Text;
                    PersonAdd.shenasname = textBox4.Text;
                    PersonAdd.nationalcode = textBox5.Text;
                    PersonAdd.sarparast = textBox9.Text;
                    PersonAdd.tahsilat = textBox10.Text;
                    PersonAdd.tell = textBox11.Text;
                    PersonAdd.shomarehesab = textBox12.Text;
                    //------------------------- B Date  ---------------------------------//
                    string da3 = (comboBox6.SelectedIndex + 1).ToString("00");//
                    string mon3 = (comboBox7.SelectedIndex + 1).ToString("00");//
                    string yea3 = (comboBox8.SelectedIndex + 1310).ToString("0000");

                    try
                    {
                        string dat3 = yea3 + "/" + mon3 + "/" + da3;
                        PersonAdd.bdate = Convert.ToDateTime(dat3);
                    }
                    catch (Exception ex)
                    {
                        string datealaki2 = "1400/12/31";
                        PersonAdd.bdate = Convert.ToDateTime(datealaki2);
                    }
                    //-----------------------------------------------------------------//

                    //------------------------- Child ---------------------------------//
                    if (textBox13.Text == "" || textBox11.Text == null)
                    {
                        PersonAdd.child = 0;
                    }
                    else
                    {
                        PersonAdd.child = Convert.ToByte(textBox13.Text);
                    }
                    //------------------------- ComboS --------------------------//
                    PersonAdd.vaziyat = Convert.ToByte(comboBox1.SelectedValue);
                    PersonAdd.noekomak_id = Convert.ToByte(comboBox3.SelectedValue);
                    PersonAdd.noeposhesh_id = Convert.ToByte(comboBox4.SelectedValue);
                    PersonAdd.tahol_id = Convert.ToByte(comboBox5.SelectedValue);
                    //-------------------------------------------------------------//
                    PersonAdd.address = textBox14.Text;
                    PersonAdd.tozihat = textBox15.Text;
                    //^^^^^^^^^^^^^^^^^^^^^^ Koli ===================================//
                    PersonAdd.noemaloliyat = textBox23.Text;
                    PersonAdd.vasile = textBox25.Text;
                    //================================= Save ====================================//
                    try
                    {

                        context1.People.Add(PersonAdd);
                        context1.SaveChanges();
                        this.Close();
                        MessageBox.Show("معلول جدید افروزده شد");

                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message, " Khata !!!! ");
                    }
                    break;
                case 0:
                case 4:
                case 5:
                    //=================================  Koli ===================================//
                    PersonAdd.name = textBox1.Text;
                    PersonAdd.lname = textBox2.Text;
                    PersonAdd.faname = textBox3.Text;
                    PersonAdd.shenasname = textBox4.Text;
                    PersonAdd.nationalcode = textBox5.Text;
                    PersonAdd.sarparast = textBox9.Text;
                    PersonAdd.tahsilat = textBox10.Text;
                    PersonAdd.tell = textBox11.Text;
                    PersonAdd.shomarehesab = textBox12.Text;
                    //------------------------- B Date  ---------------------------------//
                    string da = (comboBox6.SelectedIndex + 1).ToString("00");//
                    string mon = (comboBox7.SelectedIndex + 1).ToString("00");//
                    string yea = (comboBox8.SelectedIndex + 1310).ToString("0000");

                    try
                    {
                        string dat = yea + "/" + mon + "/" + da;
                        PersonAdd.bdate = Convert.ToDateTime(dat);
                    }
                    catch (Exception ex)
                    {
                        string datealaki3 = "1400/12/31";
                        PersonAdd.bdate = Convert.ToDateTime(datealaki3);
                    }
                    //-----------------------------------------------------------------//
                    //string dat = "1400/01/01";
                    //PersonAdd.bdate = Convert.ToDateTime(dat);


                    //-----------------------------------------------------------------//

                    //------------------------- Child ---------------------------------//
                    if (textBox13.Text == "" || textBox11.Text == null)
                    {
                        PersonAdd.child = (byte) 0;
                    }
                    else
                    {
                        PersonAdd.child = Convert.ToByte(textBox13.Text);
                    }
                    //------------------------- ComboS --------------------------//
                    PersonAdd.vaziyat = Convert.ToByte(comboBox1.SelectedValue);
                    PersonAdd.noekomak_id = Convert.ToByte(comboBox3.SelectedValue);
                    PersonAdd.noeposhesh_id = Convert.ToByte(comboBox4.SelectedValue);
                    PersonAdd.tahol_id = Convert.ToByte(comboBox5.SelectedValue);
                    //-------------------------------------------------------------//
                    PersonAdd.address = textBox14.Text;
                    PersonAdd.tozihat = textBox15.Text;
                    //^^^^^^^^^^^^^^^^^^^^^^ Koli ===================================//
                    //================================= Save ====================================//
                    try
                    {

                        context1.People.Add(PersonAdd);
                        context1.SaveChanges();
                        this.Close();
                        MessageBox.Show("فرد جدید افروزده شد");

                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message, " Khata !!!! ");
                    }
                    break;
                ////==============================================================================//

            }

            //////////PersonAdd.hamiiiiiii   ////////////حذف
        }


        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }
       
        //==============================================================================//
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!((char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Space) || (e.KeyChar == (char)Keys.Back)))
            {
                e.Handled = true;
            }
        }
        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!((char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Space) || (e.KeyChar == (char)Keys.Back)))
            {
                e.Handled = true;
            }
        }
        private void textBox3_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (!((char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Space) || (e.KeyChar == (char)Keys.Back)))
            {
                e.Handled = true;
            }
        }
        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }
        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }
        private void textBox9_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!((char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Space) || (e.KeyChar == (char)Keys.Back)))
            {
                e.Handled = true;
            }
        }

        private void textBox10_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!((char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Space) || (e.KeyChar == (char)Keys.Back)))
            {
                e.Handled = true;
            }
        }

        private void textBox11_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        private void textBox12_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        private void textBox13_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        private void textBox14_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (char.IsControl(e.KeyChar))
            //{
            //    e.Handled = true;
            //}
        }

        private void textBox15_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            //if (char.IsControl(e.KeyChar))
            //{
            //    e.Handled = true;
            //}
        }
        private void textBox16_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!((char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Space) || (e.KeyChar == (char)Keys.Back)))
            {
                e.Handled = true;
            }
        }
        private void textBox17_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (!((char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Space) || (e.KeyChar == (char)Keys.Back)))
            {
                e.Handled = true;
            }
        }

        private void textBox19_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (!((char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Space) || (e.KeyChar == (char)Keys.Back)))
            {
                e.Handled = true;
            }
        }

        private void textBox22_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!((char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Space) || (e.KeyChar == (char)Keys.Back)))
            {
                e.Handled = true;
            }
        }

        private void comboBox6_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {


        }
        private void textBox8_TextChanged_1(object sender, EventArgs e)
        {
            //    LittleEntity Queryy = new LittleEntity();
            //    if (textBox8.Text == "" || textBox8.Text == null)
            //    {
            //        dataGridView1afrad.DataSource = Queryy.People.ToList();
            //    }
            //    else
            //    {
            //        var afradlinq = Queryy.People.ToList();
            //        var af = from b in afradlinq
            //                 where (b.name.Contains(textBox1.Text) || b.lname.Contains(textBox1.Text))
            //                 select new
            //                 {
            //                     Name = b.name + " " + b.lname,
            //                     b.nationalcode,
            //                     bnoekomak = b.BNoekomak.title,
            //                     b.id,
            //                     b.shenasname,
            //                     b.bdate,
            //                     b.faname,
            //                     b.vasile,
            //                     b.child,
            //                     b.sarparast,
            //                     b.tahsilat,
            //                     b.address,
            //                     b.tell,
            //                     b.shomarehesab,
            //                     b.uni,
            //                     b.reshte,
            //                     bnoeposhesh = b.BNoeposhesh.title,
            //                     b.datevorod,
            //                     b.maghta,
            //                     b.shahr,
            //                     b.madrese,
            //                     b.noemaloliyat,
            //                     b.tozihat,
            //                     b.BVaziyat.title
            //                 };
            //        dataGridView1afrad.DataSource = af.ToList();
        }

        private void textBox7_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!((char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Space) || (e.KeyChar == (char)Keys.Back)))
            {
                e.Handled = true;
            }
        }

        private void textBox20_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!((char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Space) || (e.KeyChar == (char)Keys.Back)))
            {
                e.Handled = true;
            }
        }

        private void textBox21_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!((char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Space) || (e.KeyChar == (char)Keys.Back)))
            {
                e.Handled = true;
            }
        }

        private void textBox23_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!((char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Space) || (e.KeyChar == (char)Keys.Back)))
            {
                e.Handled = true;
            }
        }

        private void textBox25_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!((char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Space) || (e.KeyChar == (char)Keys.Back)))
            {
                e.Handled = true;
            }
        }
       //================================= Del All ================================//
        private void button2_Click_2(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox9.Text = "";
            textBox10.Text = "";
            textBox11.Text = "";
            textBox12.Text = "";
            textBox13.Text = "";
            textBox14.Text = "";
            textBox15.Text = "";
            textBox16.Text = "";
            textBox17.Text = "";
            textBox19.Text = "";
            textBox20.Text = "";
            textBox21.Text = "";
            textBox22.Text = "";
            textBox23.Text = "";
            textBox25.Text = "";
            textBox7.Text = "";
        }
        //=========================================================================//
       
        

        //private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        //{

        //}
    }

}
//switch (comboBox1.SelectedIndex)
//           {

//               case 1:
//                   KoliBox.Visible = true;
//                   Daneshjobox.Visible = true;
//                   Daneshamozbox.Visible = false;
//                   malolbox.Visible = false;
//                   //BimarBox.Visible=false;
//                   break;
//               case 2:
//                   KoliBox.Visible = true;
//                   Daneshjobox.Visible = false;
//                   Daneshamozbox.Visible = true;
//                   malolbox.Visible = false;
//                  // BimarBox.Visible=false;
//                   break;
//               case 3:
//                   KoliBox.Visible = true;
//                   Daneshjobox.Visible = false;
//                   Daneshamozbox.Visible = false;
//                   malolbox.Visible = true;
//                //   BimarBox.Visible=false;
//                   break;
//               case 4:
//                   KoliBox.Visible = true;
//                   Daneshjobox.Visible = false;
//                   Daneshamozbox.Visible = false;
//                   malolbox.Visible = false;
//                  // BimarBox.Visible=true;
//                   break;
//               case 0:
//               case 5:
//                    KoliBox.Visible = true;
//                   Daneshjobox.Visible = false;
//                   Daneshamozbox.Visible = false;
//                   malolbox.Visible = false;
//                   //BimarBox.Visible=false;
//                   break;
