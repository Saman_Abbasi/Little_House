﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Little_House__
{
    public partial class hasbdate : DevComponents.DotNetBar.Metro.MetroForm
    {
       public LittleEntity queryy = new LittleEntity();
       
        public hasbdate()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
        //    select new { Name = b.Hami.name + " " + b.Hami.lname, namefard = b.Person.name + " " + b.Person.lname, mablagh = Convert.ToInt32(b.mablagh), b.sharh, b.tozihat, b.datetime };
        //    dataGridView1.DataSource = w.ToList();
            if (maskedTextBox2.Text == "" || maskedTextBox2.Text == null)
            {
                var hasbdate =queryy.Tarakoneshes.ToList();
                var q = from b in hasbdate.ToList()
                        where (b.datetime >= Convert.ToDateTime (maskedTextBox1.Text))
                        select new { Name = b.Hami.name + " " + b.Hami.lname, namefard = b.Person.name + " " + b.Person.lname, mablagh = (Convert.ToInt32(b.mablagh)), b.sharh, b.tozihat, b.datetime };
                dataGridView1.DataSource = q.ToList();
            }

            else if ((maskedTextBox2.Text != "" || maskedTextBox2.Text != null))
            {


                var hasbedate1 = queryy.Tarakoneshes.ToList();
                var q = from b in hasbedate1.ToList()
                        where (b.datetime >= Convert.ToDateTime (maskedTextBox1.Text)) && (b.datetime <= Convert.ToDateTime(maskedTextBox2.Text))

                        select new { Name = b.Hami.name + " " + b.Hami.lname, namefard = b.Person.name + " " + b.Person.lname, mablagh = (Convert.ToInt32(b.mablagh)), b.sharh, b.tozihat, b.datetime };
                dataGridView1.DataSource = q.ToList();


            }

        }

        private void hasbdate_Load(object sender, EventArgs e)
        {
           // var taradate = queryy.Tarakoneshes.ToList();
            var w = from b in queryy.Tarakoneshes.ToList()
                    select new { Name = b.Hami.name + " " + b.Hami.lname, namefard = b.Person.name + " " + b.Person.lname, mablagh = Convert.ToInt32(b.mablagh), b.sharh, b.tozihat, b.datetime };
            dataGridView1.DataSource = w.ToList();
        }
    }
}
