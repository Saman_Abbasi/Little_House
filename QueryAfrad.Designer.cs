﻿namespace Little_House__
{
    partial class QueryAfrad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1afrad = new System.Windows.Forms.DataGridView();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.faname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tahsilat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shomarehesab = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reshte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nationalcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shenasname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.child = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sarparast = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uni = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.noekomak = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.noeposhesh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datevorod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maghta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shahr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.madrese = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.noemaloliyat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vasile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vaziyat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tahol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tell = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tozihat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1afrad)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1afrad
            // 
            this.dataGridView1afrad.AccessibleRole = System.Windows.Forms.AccessibleRole.RowHeader;
            this.dataGridView1afrad.AllowUserToAddRows = false;
            this.dataGridView1afrad.AllowUserToDeleteRows = false;
            this.dataGridView1afrad.AllowUserToResizeRows = false;
            this.dataGridView1afrad.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1afrad.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.name,
            this.faname,
            this.tahsilat,
            this.shomarehesab,
            this.reshte,
            this.id,
            this.nationalcode,
            this.shenasname,
            this.bdate,
            this.child,
            this.sarparast,
            this.uni,
            this.noekomak,
            this.noeposhesh,
            this.datevorod,
            this.maghta,
            this.shahr,
            this.madrese,
            this.noemaloliyat,
            this.vasile,
            this.ax,
            this.vaziyat,
            this.tahol,
            this.tell,
            this.tozihat,
            this.address});
            this.dataGridView1afrad.Location = new System.Drawing.Point(621, 48);
            this.dataGridView1afrad.MultiSelect = false;
            this.dataGridView1afrad.Name = "dataGridView1afrad";
            this.dataGridView1afrad.ReadOnly = true;
            this.dataGridView1afrad.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dataGridView1afrad.RowHeadersVisible = false;
            this.dataGridView1afrad.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1afrad.Size = new System.Drawing.Size(552, 309);
            this.dataGridView1afrad.TabIndex = 16;
            this.dataGridView1afrad.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1afrad_CellContentClick);
            // 
            // name
            // 
            this.name.DataPropertyName = "name";
            this.name.HeaderText = "نام و نام خانوادگی";
            this.name.Name = "name";
            this.name.ReadOnly = true;
            this.name.Width = 150;
            // 
            // faname
            // 
            this.faname.DataPropertyName = "faname";
            this.faname.HeaderText = "نام پدر";
            this.faname.Name = "faname";
            this.faname.ReadOnly = true;
            // 
            // tahsilat
            // 
            this.tahsilat.DataPropertyName = "tahsilat";
            this.tahsilat.HeaderText = "تحصیلات";
            this.tahsilat.Name = "tahsilat";
            this.tahsilat.ReadOnly = true;
            // 
            // shomarehesab
            // 
            this.shomarehesab.DataPropertyName = "shomarehesab";
            this.shomarehesab.HeaderText = "شماره حساب";
            this.shomarehesab.Name = "shomarehesab";
            this.shomarehesab.ReadOnly = true;
            // 
            // reshte
            // 
            this.reshte.DataPropertyName = "reshte";
            this.reshte.HeaderText = "رشته تحصیلی";
            this.reshte.Name = "reshte";
            this.reshte.ReadOnly = true;
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            // 
            // nationalcode
            // 
            this.nationalcode.DataPropertyName = "nationalcode";
            this.nationalcode.HeaderText = "کد ملی";
            this.nationalcode.Name = "nationalcode";
            this.nationalcode.ReadOnly = true;
            // 
            // shenasname
            // 
            this.shenasname.DataPropertyName = "shenasname";
            this.shenasname.HeaderText = "شماره شناسنامه";
            this.shenasname.Name = "shenasname";
            this.shenasname.ReadOnly = true;
            // 
            // bdate
            // 
            this.bdate.DataPropertyName = "bdate";
            this.bdate.HeaderText = "تاریخ تولد";
            this.bdate.Name = "bdate";
            this.bdate.ReadOnly = true;
            // 
            // child
            // 
            this.child.DataPropertyName = "child";
            this.child.HeaderText = "تعداد فرزندان";
            this.child.Name = "child";
            this.child.ReadOnly = true;
            // 
            // sarparast
            // 
            this.sarparast.DataPropertyName = "sarparast";
            this.sarparast.HeaderText = "سرپرست خانواده";
            this.sarparast.Name = "sarparast";
            this.sarparast.ReadOnly = true;
            // 
            // uni
            // 
            this.uni.DataPropertyName = "uni";
            this.uni.HeaderText = "دانشگاه";
            this.uni.Name = "uni";
            this.uni.ReadOnly = true;
            // 
            // noekomak
            // 
            this.noekomak.DataPropertyName = "BNoekomak";
            this.noekomak.HeaderText = "نوع کمک دریافتی";
            this.noekomak.Name = "noekomak";
            this.noekomak.ReadOnly = true;
            // 
            // noeposhesh
            // 
            this.noeposhesh.DataPropertyName = "bnoeposhesh";
            this.noeposhesh.HeaderText = "نوع پوشش";
            this.noeposhesh.Name = "noeposhesh";
            this.noeposhesh.ReadOnly = true;
            // 
            // datevorod
            // 
            this.datevorod.DataPropertyName = "datevorod";
            this.datevorod.HeaderText = "تاریخ ورود به دانشگاه";
            this.datevorod.Name = "datevorod";
            this.datevorod.ReadOnly = true;
            // 
            // maghta
            // 
            this.maghta.DataPropertyName = "maghta";
            this.maghta.HeaderText = "مقطع تحصیلی";
            this.maghta.Name = "maghta";
            this.maghta.ReadOnly = true;
            // 
            // shahr
            // 
            this.shahr.DataPropertyName = "shahr";
            this.shahr.HeaderText = "شهر محل تحصیل";
            this.shahr.Name = "shahr";
            this.shahr.ReadOnly = true;
            // 
            // madrese
            // 
            this.madrese.DataPropertyName = "madrese";
            this.madrese.HeaderText = "نام مدرسه";
            this.madrese.Name = "madrese";
            this.madrese.ReadOnly = true;
            // 
            // noemaloliyat
            // 
            this.noemaloliyat.DataPropertyName = "noemaloliyat";
            this.noemaloliyat.HeaderText = "نوع معلولیت";
            this.noemaloliyat.Name = "noemaloliyat";
            this.noemaloliyat.ReadOnly = true;
            // 
            // vasile
            // 
            this.vasile.DataPropertyName = "vasile";
            this.vasile.HeaderText = "وسیله توانبخشی";
            this.vasile.Name = "vasile";
            this.vasile.ReadOnly = true;
            // 
            // ax
            // 
            this.ax.DataPropertyName = "ax";
            this.ax.HeaderText = "عکس";
            this.ax.Name = "ax";
            this.ax.ReadOnly = true;
            // 
            // vaziyat
            // 
            this.vaziyat.DataPropertyName = "vaziyat";
            this.vaziyat.HeaderText = "نوع";
            this.vaziyat.Name = "vaziyat";
            this.vaziyat.ReadOnly = true;
            // 
            // tahol
            // 
            this.tahol.DataPropertyName = "tahol";
            this.tahol.HeaderText = "تاهل";
            this.tahol.Name = "tahol";
            this.tahol.ReadOnly = true;
            // 
            // tell
            // 
            this.tell.DataPropertyName = "tell";
            this.tell.HeaderText = "شماره تماس";
            this.tell.Name = "tell";
            this.tell.ReadOnly = true;
            // 
            // tozihat
            // 
            this.tozihat.DataPropertyName = "tozihat";
            this.tozihat.HeaderText = "توضیحات";
            this.tozihat.Name = "tozihat";
            this.tozihat.ReadOnly = true;
            // 
            // address
            // 
            this.address.DataPropertyName = "address";
            this.address.HeaderText = "آدرس";
            this.address.Name = "address";
            this.address.ReadOnly = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(781, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(209, 20);
            this.textBox1.TabIndex = 17;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1024, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "نام شخص مورد نظر را وارد کنید";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerColorTint = System.Drawing.Color.White;
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.OfficeMobile2014;
            this.styleManager1.MetroColorParameters = new DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(71)))), ((int)(((byte)(42))))));
            // 
            // QueryAfrad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1185, 534);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.dataGridView1afrad);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "QueryAfrad";
            this.Text = " ";
            this.Load += new System.EventHandler(this.QueryX_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1afrad)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1afrad;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn faname;
        private System.Windows.Forms.DataGridViewTextBoxColumn tahsilat;
        private System.Windows.Forms.DataGridViewTextBoxColumn shomarehesab;
        private System.Windows.Forms.DataGridViewTextBoxColumn reshte;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn nationalcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn shenasname;
        private System.Windows.Forms.DataGridViewTextBoxColumn bdate;
        private System.Windows.Forms.DataGridViewTextBoxColumn child;
        private System.Windows.Forms.DataGridViewTextBoxColumn sarparast;
        private System.Windows.Forms.DataGridViewTextBoxColumn uni;
        private System.Windows.Forms.DataGridViewTextBoxColumn noekomak;
        private System.Windows.Forms.DataGridViewTextBoxColumn noeposhesh;
        private System.Windows.Forms.DataGridViewTextBoxColumn datevorod;
        private System.Windows.Forms.DataGridViewTextBoxColumn maghta;
        private System.Windows.Forms.DataGridViewTextBoxColumn shahr;
        private System.Windows.Forms.DataGridViewTextBoxColumn madrese;
        private System.Windows.Forms.DataGridViewTextBoxColumn noemaloliyat;
        private System.Windows.Forms.DataGridViewTextBoxColumn vasile;
        private System.Windows.Forms.DataGridViewTextBoxColumn ax;
        private System.Windows.Forms.DataGridViewTextBoxColumn vaziyat;
        private System.Windows.Forms.DataGridViewTextBoxColumn tahol;
        private System.Windows.Forms.DataGridViewTextBoxColumn tell;
        private System.Windows.Forms.DataGridViewTextBoxColumn tozihat;
        private System.Windows.Forms.DataGridViewTextBoxColumn address;
        private DevComponents.DotNetBar.StyleManager styleManager1;
    }
}