﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Little_House__
{
    public partial class HasbFard : DevComponents.DotNetBar.Metro.MetroForm
    {
        LittleEntity tarafard = new LittleEntity();
        public HasbFard()
        {
            InitializeComponent();
        }

        private void HasbFard_Load(object sender, EventArgs e)
        {
            var w = from b in tarafard.Tarakoneshes.ToList()
                    select new { Name = b.Hami.name + " " + b.Hami.lname, namefard = b.Person.name + " " + b.Person.lname, b.mablagh, b.sharh, b.tozihat, b.datetime };
            dataGridView2.DataSource = w.ToList();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
            var tarafardx = tarafard.Tarakoneshes.ToList();


            var q = from a in tarafardx.ToList()
                    where a.Person.name.Contains(textBox1.Text) || a.Person.lname.Contains(textBox1.Text)
                    select new { Name = a.Hami.name + " " + a.Hami.lname, namefard = a.Person.name + " " + a.Person.lname, a.mablagh, a.sharh, a.tozihat, a.datetime };
            dataGridView2.DataSource = q.ToList();
        }
    }
}
