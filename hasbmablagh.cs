﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Little_House__
{
    public partial class hasbmablagh : DevComponents.DotNetBar.Metro.MetroForm
    {
        decimal number;
        decimal number2;
        LittleEntity taramablagh = new LittleEntity();
        public hasbmablagh()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (maskedTextBox2.Text == "" || maskedTextBox2.Text == null)
            {
                var hasbmablagh1 = taramablagh.Tarakoneshes.ToList();
                var q = from b in hasbmablagh1.ToList()
                        where (b.mablagh >= Convert.ToInt32 (maskedTextBox1.Text))

                        select new { Name = b.Hami.name + " " + b.Hami.lname, namefard = b.Person.name + " " + b.Person.lname, mablagh = (Convert.ToInt32(b.mablagh)), b.sharh, b.tozihat, b.datetime };
                dataGridView1.DataSource = q.ToList();
            }

            else if ((maskedTextBox2.Text != "" || maskedTextBox2.Text != null))
            {


                var hasbmablagh1 = taramablagh.Tarakoneshes.ToList();
                var q = from b in hasbmablagh1.ToList()
                        where (b.mablagh >= Convert.ToDecimal(maskedTextBox1.Text)) && (b.mablagh <= Convert.ToDecimal(maskedTextBox2.Text))

                        select new { Name = b.Hami.name + " " + b.Hami.lname, namefard = b.Person.name + " " + b.Person.lname, mablagh = (Convert.ToInt32(b.mablagh)), b.sharh, b.tozihat, b.datetime };
                dataGridView1.DataSource = q.ToList();


            }
    //        else if (maskedTextBox1.Text ==""||maskedTextBox1.Text==null && maskedTextBox2.Text==""||maskedTextBox2.Text==null)
    //        {
    //           var w = from b in taramablagh.Tarakoneshes.ToList()
    //                select new { Name = b.Hami.name + " " + b.Hami.lname, namefard = b.Person.name + " " + b.Person.lname,mablagh = Convert.ToInt32(b.mablagh), b.sharh, b.tozihat, b.datetime };
    //        dataGridView1.DataSource = w.ToList();  
    //        }
        }

        private void hasbmablagh_Load(object sender, EventArgs e)
        {
            var w = from b in taramablagh.Tarakoneshes.ToList()
                    select new { Name = b.Hami.name + " " + b.Hami.lname, namefard = b.Person.name + " " + b.Person.lname,mablagh = Convert.ToInt32(b.mablagh), b.sharh, b.tozihat, b.datetime };
            dataGridView1.DataSource = w.ToList();
        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            
        }

        private void maskedTextBox2_TextChanged(object sender, EventArgs e)
        {
             //decimal number;
             if (decimal.TryParse(maskedTextBox2.Text, out number2))
             {
                 maskedTextBox2.Text = string.Format("{0:n0}", number2);
                 maskedTextBox2.SelectionStart = maskedTextBox2.Text.Length;
             }
        }

        private void maskedTextBox1_TextChanged(object sender, EventArgs e)
        {
            //decimal number;
            if (decimal.TryParse(maskedTextBox1.Text, out number))
            {
                maskedTextBox1.Text = string.Format("{0:n0}", number);
                maskedTextBox1.SelectionStart = maskedTextBox1.Text.Length;
            }
        }
    }
}
