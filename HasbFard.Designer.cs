﻿namespace Little_House__
{
    partial class HasbFard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.namefard = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.namehami = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mablagh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sharh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tozihat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(436, 45);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(243, 20);
            this.textBox1.TabIndex = 1;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.namefard,
            this.namehami,
            this.mablagh,
            this.date,
            this.sharh,
            this.tozihat});
            this.dataGridView2.Location = new System.Drawing.Point(12, 96);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.Size = new System.Drawing.Size(808, 217);
            this.dataGridView2.TabIndex = 2;
            // 
            // namefard
            // 
            this.namefard.DataPropertyName = "namefard";
            this.namefard.HeaderText = "نام و نام خانوادگی فرد";
            this.namefard.Name = "namefard";
            this.namefard.ReadOnly = true;
            this.namefard.Width = 140;
            // 
            // namehami
            // 
            this.namehami.DataPropertyName = "name";
            this.namehami.HeaderText = "نام و نام خانوادگی حامی";
            this.namehami.Name = "namehami";
            this.namehami.ReadOnly = true;
            this.namehami.Width = 150;
            // 
            // mablagh
            // 
            this.mablagh.DataPropertyName = "mablagh";
            this.mablagh.HeaderText = "مبلغ واریز شده";
            this.mablagh.Name = "mablagh";
            this.mablagh.ReadOnly = true;
            // 
            // date
            // 
            this.date.DataPropertyName = "datetime";
            this.date.HeaderText = "تاریخ واریز";
            this.date.Name = "date";
            this.date.ReadOnly = true;
            // 
            // sharh
            // 
            this.sharh.DataPropertyName = "sharh";
            this.sharh.HeaderText = "شرح";
            this.sharh.Name = "sharh";
            this.sharh.ReadOnly = true;
            // 
            // tozihat
            // 
            this.tozihat.DataPropertyName = "tozihat";
            this.tozihat.HeaderText = "توضیحات";
            this.tozihat.Name = "tozihat";
            this.tozihat.ReadOnly = true;
            this.tozihat.Width = 200;
            // 
            // HasbFard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(858, 395);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.textBox1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "HasbFard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HasbFard";
            this.Load += new System.EventHandler(this.HasbFard_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn namefard;
        private System.Windows.Forms.DataGridViewTextBoxColumn namehami;
        private System.Windows.Forms.DataGridViewTextBoxColumn mablagh;
        private System.Windows.Forms.DataGridViewTextBoxColumn date;
        private System.Windows.Forms.DataGridViewTextBoxColumn sharh;
        private System.Windows.Forms.DataGridViewTextBoxColumn tozihat;
    }
}