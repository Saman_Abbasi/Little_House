﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Little_House__
{
    public partial class PardakhtX : DevComponents.DotNetBar.Metro.MetroForm
    {
        decimal number;
        LittleEntity contex = new LittleEntity();

        public PardakhtX()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            
            Tarakonesh tarakoneshsabt = new Tarakonesh();
            //-------------------------------------------------------------//
            //if (textBox1.Text == "")
            //{
            //    break;

            //}
            //else
            //{
            tarakoneshsabt.mablagh = number;
            //}

            //-------------------------------------------------------------//
            //-------------------------------------------------------------// 
            PersianCalendar persian = new PersianCalendar();
            string day; string month; string year;
            if (textBox6.Text == "")
            { day = persian.GetDayOfMonth(DateTime.Now).ToString("00"); }
            else { day = textBox6.Text; }

            if (textBox5.Text == "")
            { month = persian.GetMonth(DateTime.Now).ToString("00"); }
            else { month = textBox5.Text; }

            if (textBox4.Text == "")
            { year = persian.GetYear(DateTime.Now).ToString("0000"); }
            else { year = textBox4.Text; }

            var pernow = year + "/" + month + "/" + day;
            tarakoneshsabt.datetime = Convert.ToDateTime(pernow);
            //-------------------------------------------------------------//
            //-------------------------------------------------------------//
            tarakoneshsabt.tozihat = textBox3.Text;
            tarakoneshsabt.sharh = textBox2.Text;
            //-------------------------------------------------------------//
            //----------------------- ComBoooo -------------------------//

            //tarakoneshsabt.hami_id=Convert.ToInt32( comboBox3.SelectedValue);
            //tarakoneshsabt.person_id =Convert.ToInt32( comboBox4.SelectedValue);
            tarakoneshsabt.person_id = 14;//  Convert.ToInt32(comboBox4.ValueMember);
            tarakoneshsabt.hami_id = 1015;//   Convert.ToInt32(comboBox3.ValueMember);

            //-------------------------------------------------------------//
            //-------------------------------------------------------------//
            try
            {
                contex.Tarakoneshes.Add(tarakoneshsabt);
                contex.SaveChanges();
                this.Close();
                MessageBox.Show("تراکنش ثبت شد");
            }
            catch (Exception)
            {

                MessageBox.Show("اشکال در ارتباط با دیتابیس", "خطا");
            }
        }

        private void PardakhtX_Load(object sender, EventArgs e)
        {
            //comboBox3.DataSource = contex.FullHamiProc().ToList();
            
            //comboBox4.DataSource = contex.FullPersonProc().ToList();

            // TODO: This line of code loads data into the '_little_houseDataSet.Hami' table. You can move, or remove it, as needed.
            ////////////this.hamiTableAdapter.Fill(this._little_houseDataSet.Hami);/////////
            //comboBox3.DataSource = hamiBindingSource;
            //comboBox3.DisplayMember = "name";
            LittleEntity HamiCombo = new LittleEntity();
            //var HamiCombo2 = HamiCombo.Hamis.ToList();
            //var com2 = (from a in HamiCombo2 select new { HName = a.name + " " + a.lname });
            //comboBox3.DataSource = com2.ToList();
            //comboBox3.DisplayMember = "HName";
            //comboBox3.ValueMember = "id";
            //LittleEntity PersonCombo = new LittleEntity();
            //var q = PersonCombo.People.Select(x => new { name = x.name + " " + x.lname }).ToList();
            //comboBox4.DataSource = q;//.ToList();
            //comboBox4.DisplayMember = "name";
            //comboBox4.ValueMember = "id";
            var tar = HamiCombo.Tarakoneshes.ToList();
            var test = (from t in tar
                       select new {hamnam= t.Hami.name + " "+t.Hami.lname, pernam = t.Person.name+" "+t.Person.lname});//.ToList();

            comboBox3.DataSource = test.ToList();
           
            var test2 = (from t in tar
                         select new { pernam = t.Person.name + " " + t.Person.lname });//.ToList();
            comboBox4.DataSource = test2.ToList();
            comboBox4.DisplayMember = "pernam";
            comboBox4.ValueMember = "id";
            comboBox3.DisplayMember = "hamnam";
            comboBox3.ValueMember = "id";
            //LittleEntity HamiCombo = new LittleEntity();
            //var tar = HamiCombo.Tarakoneshes.ToList();
            //var test = (from t in tar select new { hamnam = t.Hami.name + " " + t.Hami.lname }).ToList();

            //comboBox3.DataSource = test;
            //comboBox3.DisplayMember = "hamnam";
            //comboBox3.ValueMember = "id";
            //var test2 = (from t in tar select new { pernam = t.Person.name + " " + t.Person.lname }).ToList();
            //comboBox4.DataSource = test;
            //comboBox4.DisplayMember = "pernam";
            //comboBox4.ValueMember = "id";
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void maskedTextBox2_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (decimal.TryParse(textBox1.Text, out number))
            {
                textBox1.Text = string.Format("{0:n0}", number);
                textBox1.SelectionStart = textBox1.Text.Length;

            }
        }

        private void comboBox3_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            
        }

        private void comboBox3_MouseDown(object sender, MouseEventArgs e)
        {
            
        }

     
    }
}
