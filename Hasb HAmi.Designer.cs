﻿namespace Little_House__
{
    partial class Hasb_HAmi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.namehami = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.namefard = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mablagh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sharh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tozihat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBox1 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.namehami,
            this.namefard,
            this.mablagh,
            this.date,
            this.sharh,
            this.tozihat});
            this.dataGridView1.Location = new System.Drawing.Point(12, 133);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(808, 217);
            this.dataGridView1.TabIndex = 0;
            // 
            // namehami
            // 
            this.namehami.DataPropertyName = "name";
            this.namehami.HeaderText = "نام و نام خانوادگی حامی";
            this.namehami.Name = "namehami";
            this.namehami.ReadOnly = true;
            this.namehami.Width = 150;
            // 
            // namefard
            // 
            this.namefard.DataPropertyName = "namefard";
            this.namefard.HeaderText = "نام و نام خانوادگی فرد";
            this.namefard.Name = "namefard";
            this.namefard.ReadOnly = true;
            this.namefard.Width = 140;
            // 
            // mablagh
            // 
            this.mablagh.DataPropertyName = "mablagh";
            this.mablagh.HeaderText = "مبلغ واریز شده";
            this.mablagh.Name = "mablagh";
            this.mablagh.ReadOnly = true;
            // 
            // date
            // 
            this.date.DataPropertyName = "datetime";
            this.date.HeaderText = "تاریخ واریز";
            this.date.Name = "date";
            this.date.ReadOnly = true;
            // 
            // sharh
            // 
            this.sharh.DataPropertyName = "sharh";
            this.sharh.HeaderText = "شرح";
            this.sharh.Name = "sharh";
            this.sharh.ReadOnly = true;
            // 
            // tozihat
            // 
            this.tozihat.DataPropertyName = "tozihat";
            this.tozihat.HeaderText = "توضیحات";
            this.tozihat.Name = "tozihat";
            this.tozihat.ReadOnly = true;
            this.tozihat.Width = 200;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(427, 61);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(297, 20);
            this.textBox1.TabIndex = 1;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // Hasb_HAmi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(832, 447);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.dataGridView1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Hasb_HAmi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hasb_HAmi";
            this.Load += new System.EventHandler(this.Hasb_HAmi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn namehami;
        private System.Windows.Forms.DataGridViewTextBoxColumn namefard;
        private System.Windows.Forms.DataGridViewTextBoxColumn mablagh;
        private System.Windows.Forms.DataGridViewTextBoxColumn date;
        private System.Windows.Forms.DataGridViewTextBoxColumn sharh;
        private System.Windows.Forms.DataGridViewTextBoxColumn tozihat;

    }
}