﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Linq;
using System.Threading.Tasks;


namespace Little_House__
{
    public partial class EditHami : DevComponents.DotNetBar.Metro.MetroForm
    {
        string idid;
        LittleEntity EditHamiQueryy = new LittleEntity();
        public EditHami()
        {
            InitializeComponent();
        }

        private void EditHami_Load(object sender, EventArgs e)
        {
            string d = textBox1.Text;
            idid = d;
            var HamiMainLoad = EditHamiQueryy.Hamis.ToList();
            var qHL = from bHL in HamiMainLoad.ToList()
                      where bHL.id.Equals(Convert.ToInt32(d))
                      select new { bHL.name, bHL.lname, bHL.tell, bHL.id, bHL.tozihat, bHL.address };
            dataGridViewX1.DataSource = qHL.ToList();
            textBox1.Text = dataGridViewX1.CurrentRow.Cells["Name"].Value.ToString();
            textBox2.Text = dataGridViewX1.CurrentRow.Cells["lname"].Value.ToString();
            textBox3.Text = dataGridViewX1.CurrentRow.Cells["tell"].Value.ToString();
            textBox4.Text = dataGridViewX1.CurrentRow.Cells["address"].Value.ToString();
            richTextBox1.Text = dataGridViewX1.CurrentRow.Cells["tozihat"].Value.ToString();
            textBox10.Text = dataGridViewX1.CurrentRow.Cells["id"].Value.ToString();
            textBox1.ReadOnly = true; textBox2.ReadOnly = true; textBox3.ReadOnly = true; textBox4.ReadOnly = true; richTextBox1.ReadOnly = true;
        }

        private void Editbutton_Click(object sender, EventArgs e)
        {
            textBox1.ReadOnly = false; textBox2.ReadOnly = false; textBox3.ReadOnly = false; textBox4.ReadOnly = false; richTextBox1.ReadOnly = false;
            Editbutton.Visible = false;
            Sabtbutton.Visible = true;
        }

        private void Sabtbutton_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("آیا تغییرات ذخیره شود ؟؟؟", "احتیاط", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                var na = textBox1.Text;
                //LittleEntity context = new LittleEntity();
                var EditHamiSat = EditHamiQueryy.Hamis.ToList();
                var EHS = from p in EditHamiSat
                          where p.id == Convert.ToInt32(idid)
                          select p;
                foreach (Hami p in EHS)
                {
                    p.name = textBox1.Text;
                    p.lname = textBox2.Text;
                    p.tell = textBox3.Text;
                    p.address = textBox4.Text;
                    p.tozihat = richTextBox1.Text;
                }
                try
                {
                    //EditHamiQueryy.Hamis.Add();
                    EditHamiQueryy.SaveChanges();
                    MessageBox.Show("تغییرات اعمال شد","ثبت مشخصات",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    this.Close();
                }
                catch (Exception)
                {

                    MessageBox.Show("!! اشکال در ارتباط با بانک اطلاعاتی","خطا",MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
                

                //(form.Controls["dataGridViewX2"] as DataGridView).Refresh();
                //form.Controls["dataGridViewX2"].Refresh();

                //var HamiMainLoad1 = EditHamiQueryy.Hamis.ToList();
                //var qHL = from bHL in HamiMainLoad1.ToList()
                //          select new { name = bHL.name + " " + bHL.lname, tell = bHL.tell, id = bHL.id };
                //(form.Controls["dataGridViewX2"] as DataGridView).DataSource = qHL.ToList();
              //(  form.Controls["textBox4"]as TextBox).Text = "Salaaaam";
                //Form frm = (Form)this.Parent;
                //DataGridView dt = (DataGridView)frm.Controls["dataGridViewX2"];
                //dt.DataSource = qHL.ToList();
            }
            if (result == DialogResult.No)
            {

            }
            if (result == DialogResult.Cancel)
            {
               var d1 = idid;
                var HamiMainLoad1 = EditHamiQueryy.Hamis.ToList();
                var qHL1 = from bHL1 in HamiMainLoad1.ToList()
                          where bHL1.id.Equals(Convert.ToInt32(d1))
                          select new { bHL1.name, bHL1.lname, bHL1.tell, bHL1.id, bHL1.tozihat, bHL1.address };
                dataGridViewX1.DataSource = qHL1.ToList();
                textBox1.Text = dataGridViewX1.CurrentRow.Cells["Name"].Value.ToString();
                textBox2.Text = dataGridViewX1.CurrentRow.Cells["lname"].Value.ToString();
                textBox3.Text = dataGridViewX1.CurrentRow.Cells["tell"].Value.ToString();
                textBox4.Text = dataGridViewX1.CurrentRow.Cells["address"].Value.ToString();
                richTextBox1.Text = dataGridViewX1.CurrentRow.Cells["tozihat"].Value.ToString();
                textBox10.Text = dataGridViewX1.CurrentRow.Cells["id"].Value.ToString();
                textBox1.ReadOnly = true; textBox2.ReadOnly = true; textBox3.ReadOnly = true; textBox4.ReadOnly = true; richTextBox1.ReadOnly = true;
                Sabtbutton.Visible = false;
                Editbutton.Visible = true;
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!((char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Space) || (e.KeyChar == (char)Keys.Back)))
            {
                e.Handled = true;
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

       
    }
}