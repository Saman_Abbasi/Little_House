﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Little_House__
{
    //////////GoooD/////////////////using (var db = new MyContextDB())
    ////////////////////////////////{
    ////////////////////////////////    var result = db.Books.SingleOrDefault(b => b.BookNumber == bookNumber);
    ////////////////////////////////    if (result != null)
    ////////////////////////////////    {
    ////////////////////////////////        result.SomeValue = "Some new value";
    ////////////////////////////////        db.SaveChanges();
    ////////////////////////////////    }
    ////////////////////////////////}
    public partial class Form1 : DevComponents.DotNetBar.RibbonForm
    {
        LittleEntity QueryyPerson = new LittleEntity();
        LittleEntity QueryyHami = new LittleEntity();
        LittleEntity QueryyTarakonesh = new LittleEntity();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var HamiForm = new HamiX();
            HamiForm.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            PersonX personform = new PersonX();
            personform.ShowDialog();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            PardakhtX PardakhtForm = new PardakhtX();
            PardakhtForm.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            QueryAfrad QueryForm = new QueryAfrad();
            QueryForm.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //==================================================================
            var HamiMainLoad = QueryyHami.Hamis.ToList();
            var qHL = from bHL in HamiMainLoad.ToList()
                      select new { name = bHL.name + " " + bHL.lname, tell = bHL.tell, id = bHL.id };
            dataGridViewX2.DataSource = qHL.ToList();
            //==================================================================
            LittleEntity entity = new LittleEntity();
            var testperson = entity.People.ToList();
            var q = from a in testperson.ToList()
                    select new { name = a.name + " " + a.lname, a.tell, vaziyat = a.BVaziyat.title, a.id };
            dataGridViewX1.DataSource = q.ToList();
            //==================================================================
            var TaraMainLoad = QueryyTarakonesh.Tarakoneshes.ToList();
            var qTL = from cTL in TaraMainLoad.ToList()
                      select new { name = cTL.Person.name + " " + cTL.Person.lname, fname = cTL.Hami.name + " " + cTL.Hami.lname, mablagh = string.Format("{0:n0}", cTL.mablagh), date = cTL.datetime.Year + "/" + cTL.datetime.Month + "/" + cTL.datetime.Day ,idT=cTL.id};
            dataGridViewX3.DataSource = qTL.ToList();
            //==================================================================                maskedTextBox1.Text = string.Format("{0:n0}", number);


        }

        private void جستوجوافرادToolStripMenuItem_Click(object sender, EventArgs e)
        {
            QueryAfrad QueryForm = new QueryAfrad();
            QueryForm.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void جستوجوحامیانToolStripMenuItem_Click(object sender, EventArgs e)
        {
            QueryHami QueryHamiForm = new QueryHami();
            QueryHamiForm.ShowDialog();
        }

        private void ribbonControl1_Click(object sender, EventArgs e)
        {

        }

        private void ribbonTabItem1_Click(object sender, EventArgs e)
        {

        }

        private void buttonItem1_Click(object sender, EventArgs e)
        {
            PersonX PersonForm = new PersonX();
            PersonForm.ShowDialog();
        }

        private void button4_Click_1(object sender, EventArgs e)
        {


        }

        private void ribbonBar2_ItemClick(object sender, EventArgs e)
        {

        }

        private void buttonItem15_Click(object sender, EventArgs e)
        {
            QueryAfrad QueryForm = new QueryAfrad();
            QueryForm.ShowDialog();
        }

        private void buttonItem16_Click(object sender, EventArgs e)
        {
            QueryHami QueryHamiForm = new QueryHami();
            QueryHamiForm.ShowDialog();
        }

        private void buttonItem19_Click(object sender, EventArgs e)
        {
            var HamiForm = new HamiX();
            HamiForm.ShowDialog();
        }

        private void buttonItem18_Click(object sender, EventArgs e)
        {
            PersonX personform = new PersonX();
            personform.ShowDialog();
        }

        private void buttonItem1_Click_1(object sender, EventArgs e)
        {
            PardakhtX PardakhtForm = new PardakhtX();
            PardakhtForm.ShowDialog();
        }

        private void buttonItem21_Click(object sender, EventArgs e)
        {
            var hasbhamix = new Hasb_HAmi();
            hasbhamix.ShowDialog();
        }

        private void buttonItem20_Click(object sender, EventArgs e)
        {
            var hasbfardx = new HasbFard();
            hasbfardx.ShowDialog();
        }

        private void buttonItem17_Click(object sender, EventArgs e)
        {
            var hasbmablaghx = new hasbmablagh();
            hasbmablaghx.ShowDialog();
        }

        private void buttonItem14_Click(object sender, EventArgs e)
        {
            var hasbedateX = new hasbdate();
            hasbedateX.ShowDialog();
        }

        private void button4_Click_2(object sender, EventArgs e)
        {

        }

        private void button4_Click_3(object sender, EventArgs e)
        {
            QueryAfrad frm2 = new QueryAfrad();
            frm2.TopLevel = false;
            this.groupPanel1.Controls.Add(frm2);
            frm2.Show();
        }

        private void groupPanel1_Click(object sender, EventArgs e)
        {

        }

        private void ribbonBar3_ItemClick(object sender, EventArgs e)
        {

        }

        private void dataGridViewX1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

            if (textBox1.Text == "" || textBox1.Text == null)
            {
                var testperson = QueryyPerson.People.ToList();
                var q = from a in testperson.ToList()
                        select new { name = a.name + " " + a.lname, a.tell, vaziyat = a.BVaziyat.title, a.id };
                dataGridViewX1.DataSource = q.ToList();
            }
            else
            {
                var afradlinq = QueryyPerson.People.ToList();
                var af = from b in afradlinq
                         where (b.name.Contains(textBox1.Text) || b.lname.Contains(textBox1.Text))
                         select new
                         {
                             Name = b.name + " " + b.lname,
                             vaziyat = b.BVaziyat.title,
                             b.tell,
                             b.id
                             //b.nationalcode,
                             //bnoekomak = b.BNoekomak.title,
                             //b.id,
                             //b.shenasname,
                             //b.bdate,
                             //b.faname,
                             //b.vasile,
                             //b.child,
                             //b.sarparast,
                             //b.tahsilat,
                             //b.address,
                             //b.tell,
                             //b.shomarehesab,
                             //b.uni,
                             //b.reshte,
                             //bnoeposhesh = b.BNoeposhesh.title,
                             //b.datevorod,
                             //b.maghta,
                             //b.shahr,
                             //b.madrese,
                             //b.noemaloliyat,
                             //b.tozihat,
                             //b.BVaziyat.title
                         };
                dataGridViewX1.DataSource = af.ToList();

            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            PersonX BuPersonx = new PersonX();
            BuPersonx.ShowDialog();
        }

        private void VirayeshButton7_Click(object sender, EventArgs e)
        {
            EditPerson Editper = new EditPerson();
            int Edit_id = int.Parse(dataGridViewX1.CurrentRow.Cells["id"].Value.ToString());
            Editper.Controls["textBox1"].Text = Convert.ToString(Edit_id);

            Editper.ShowDialog();
            //var select_id = (dataGridViewX1.CurrentRow.Cells["name"].Value.ToString());

            //int Edit_id = int.Parse(dataGridViewX1.CurrentRow.Cells["id"].Value.ToString());
            //label2.Text = Edit_id.ToString();
            //ApplicationSettingClassEdit_id = "sample string";
        }

        private void ViewButton5_Click(object sender, EventArgs e)
        {
            int View_id = int.Parse(dataGridViewX1.CurrentRow.Cells["id"].Value.ToString());
        }

        private void button7_Click(object sender, EventArgs e)
        {
            int Edit_id = int.Parse(dataGridViewX2.CurrentRow.Cells["idH"].Value.ToString());


            var ham = new EditHami();
            ham.Controls["textBox1"].Text = Convert.ToString(Edit_id);
            ham.ShowDialog();
            //(Application.OpenForms["EditHami"].Controls["TextBox1"] as TextBox).Text = dataGridViewX2.CurrentRow.Cells["name"].Value.ToString();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            if (textBox4.Text == "" || textBox4.Text == null)
            {
                var HamiMain = QueryyHami.Hamis.ToList();
                var qH = from bH in HamiMain.ToList()
                         select new { name = bH.name + " " + bH.lname, bH.tell, bH.id };
                dataGridViewX2.DataSource = qH.ToList();
            }
            else
            {
                var HamiMain2 = QueryyHami.Hamis.ToList();
                var qH2 = from bH2 in HamiMain2
                          where (bH2.name.Contains(textBox4.Text) || bH2.lname.Contains(textBox4.Text))
                          select new
                          {
                              Name = bH2.name + " " + bH2.lname,
                              bH2.tell,
                              bH2.id
                          };
                dataGridViewX2.DataSource = qH2.ToList();
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

            if (textBox2.Text == "" || textBox2.Text == null)
            {
                var TaraMain = QueryyTarakonesh.Tarakoneshes.ToList();//string.Format("{0:n0}", number);
                var qT = from bT in TaraMain.ToList()
                         select new { name = bT.Person.name + " " + bT.Person.lname, fname = bT.Hami.name + " " + bT.Hami.lname,mablagh=string.Format("{0:n0}", bT.mablagh), date = bT.datetime };
                dataGridViewX3.DataSource = qT.ToList();
            }
            else
            {
                var TaraMain2 = QueryyTarakonesh.Tarakoneshes.ToList();
                var qT2 = from bT2 in TaraMain2.ToList()
                          where ((bT2.Person.name.Contains(textBox2.Text) || bT2.Person.lname.Contains(textBox2.Text))) || (bT2.Hami.name.Contains(textBox2.Text) || bT2.Hami.lname.Contains(textBox2.Text))
                          select new { name = bT2.Person.name + " " + bT2.Person.lname, fname = bT2.Hami.name + " " + bT2.Hami.lname, bT2.mablagh, date = bT2.datetime };
                dataGridViewX3.DataSource = qT2.ToList();
            }
        }

        private void ViewButtonH_Click(object sender, EventArgs e)
        {
            EditHami frm = new EditHami();
            frm.Show();
        }

        private void DellButtonH_Click(object sender, EventArgs e)
        {

            //var tex = QueryyHami.Hamis.ToList();

            //int Edit_id = int.Parse(dataGridViewX2.CurrentRow.Cells["id"].Value.ToString());
            //label2.Text = Edit_id.ToString();

            //
            //var w = from r in tex.ToList()
            //        where (Convert.ToInt32 (r.id).Equals(Convert.ToInt32( Edit_id)))
            //        select new { name = r.lname ,j=r.lname} ;
            //textBox1.Text =Convert.ToString( w);

            //string s = "asadasdsdsa";
            //Application.OpenForms["Hami"].Controls["TextBox1"].Text = s.ToString();
        }

        private void ViewButtonT_Click(object sender, EventArgs e)
        {

        }

        private void EditButtonT_Click(object sender, EventArgs e)
        {
            int Edit_id = int.Parse(dataGridViewX3.CurrentRow.Cells["idT"].Value.ToString());
            var Tar = new EditTara();
            Tar.Controls["textBox3"].Text = Convert.ToString(Edit_id);
            Tar.ShowDialog();
        }

        private void AddButtonH_Click(object sender, EventArgs e)
        {
            var HamiForm = new HamiX();
            HamiForm.ShowDialog();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            var HamiMainLoad = QueryyHami.Hamis.ToList();
            var qHL = from bHL in HamiMainLoad.ToList()
                      select new { name = bHL.name + " " + bHL.lname, tell = bHL.tell, id = bHL.id };
            dataGridViewX2.DataSource = qHL.ToList();
        }

        private void ribbonControl1_Click_1(object sender, EventArgs e)
        {

        }

        private void dataGridViewX1_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void dataGridViewX1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                VirayeshButton7_Click((object)sender, (EventArgs)e);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridViewX2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button7_Click((object)sender, (EventArgs)e);
            }
        }

        private void dataGridViewX3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                EditButtonT_Click((object)sender, (EventArgs)e);
            }

        }

        private void dataGridViewX2_DoubleClick(object sender, EventArgs e)
        {
            button7_Click((object)sender, (EventArgs)e);
        }

        private void dataGridViewX3_DoubleClick(object sender, EventArgs e)
        {
            EditButtonT_Click((object)sender, (EventArgs)e);
        }

        private void dataGridViewX1_DoubleClick(object sender, EventArgs e)
        {
            VirayeshButton7_Click((object)sender, (EventArgs)e);
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!((char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Space) || (e.KeyChar == (char)Keys.Back)))
            {
                e.Handled = true;
            }
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!((char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Space) || (e.KeyChar == (char)Keys.Back)))
            {
                e.Handled = true;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!((char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Space) || (e.KeyChar == (char)Keys.Back)))
            {
                e.Handled = true;
            }
        }

        private void dataGridViewX3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void AddButtonT_Click(object sender, EventArgs e)
        {
            PardakhtX pardakh = new PardakhtX();
            pardakh.ShowDialog();
        }

        private void DellButtonT_Click(object sender, EventArgs e)
        {

            //var a = Convert.ToInt32(dataGridViewX3.CurrentRow.Cells["id"].Value);
            //SqlConnection Connection = new SqlConnection("server=.;Database=little-house;trusted_Connection = true");
            //Connection.Open();
            var f =Convert.ToInt32( dataGridViewX3.CurrentRow.Cells["idT"].Value.ToString());
            MessageBox.Show(f.ToString());
            var d= QueryyTarakonesh.Tarakoneshes.Where(a=>a.id ==19).FirstOrDefault();
            MessageBox.Show(d.ToString());
            QueryyTarakonesh.Tarakoneshes.Remove(d);
            //QueryyTarakonesh.Tarakoneshes.Remove(QueryyTarakonesh.Tarakoneshes.Where(a=>a.id ==19).FirstOrDefault());
            //Connection.Close();
            //SqlCommand command = new SqlCommand(string.Format("delete Tarakonesh where id ={0} ", Convert.ToInt32(a)), Connection);
            //var f = command.ExecuteNonQuery();
            //MessageBox.Show(f.ToString());
            //Connection.Close();
            //foreach (DataGridViewRow item in this.dataGridViewX3.SelectedRows)
            //{
            //    dataGridViewX3.Rows.RemoveAt(item.Index);
            //}
            //if (this.dataGridViewX3.SelectedRows.Count > 0)
            //{
            //    dataGridViewX3.Rows.RemoveAt(this.dataGridViewX3.SelectedRows[0].Index);
            //}   
        }

        private void ribbonPanel3_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            //==================================================================
            var HamiMainLoad = QueryyHami.Hamis.ToList();
            var qHL = from bHL in HamiMainLoad.ToList()
                      select new { name = bHL.name + " " + bHL.lname, tell = bHL.tell, id = bHL.id };
            dataGridViewX2.DataSource = qHL.ToList();
            //==================================================================
            LittleEntity entity = new LittleEntity();
            var testperson = entity.People.ToList();
            var q = from a in testperson.ToList()
                    select new { name = a.name + " " + a.lname, a.tell, vaziyat = a.BVaziyat.title, a.id };
            dataGridViewX1.DataSource = q.ToList();
            //==================================================================
            var TaraMainLoad = QueryyTarakonesh.Tarakoneshes.ToList();
            var qTL = from cTL in TaraMainLoad.ToList()
                      select new { name = cTL.Person.name + " " + cTL.Person.lname, fname = cTL.Hami.name + " " + cTL.Hami.lname, mablagh = string.Format("{0:n0}", cTL.mablagh), date = cTL.datetime.Year + "/" + cTL.datetime.Month + "/" + cTL.datetime.Day, idT = cTL.id };
            dataGridViewX3.DataSource = qTL.ToList();
        }
    }

}


//===================================================================================//
//===================================================================================//
//switch (textBox1.Text)
//{
//    case "a":
//        var q = from a in queryy.People.ToList()
//                where a.name.Contains("میلاد")
//                select new { a.name, a.lname };
//        dataGridView1.DataSource = q.ToList();
//        break;
//    case "b":
//        var q1 = from a in queryy.People.ToList()
//                where a.name.Contains("آرش")
//                select new { a.name, a.lname };
//        dataGridView1.DataSource = q1.ToList();

//        break;
//    case "c":
//        var q2 = from a in queryy.People.ToList()
//                 where a.name.Contains("w")
//                select new { a.name, a.lname };
//        dataGridView1.DataSource = q2.ToList();
//        break;
//    case "d":
//        var q3 = from a in queryy.People.ToList()
//                 where a.name.Contains("شس")
//                 select new { a.name, a.lname };
//        dataGridView1.DataSource = q3.ToList();
//        break;
//    case "e":
//        var q4= from a in queryy.People.ToList()
//                 where a.name.Contains("شهاب")
//                 select new { a.name, a.lname };
//        dataGridView1.DataSource = q4.ToList();
//        break;
//===================================================================================//
//===================================================================================//