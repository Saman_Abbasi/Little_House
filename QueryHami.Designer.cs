﻿namespace Little_House__
{
    partial class QueryHami
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView2hami = new System.Windows.Forms.DataGridView();
            this.name1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.telll = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tozihatt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addresss = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2hami)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView2hami
            // 
            this.dataGridView2hami.AllowUserToAddRows = false;
            this.dataGridView2hami.AllowUserToDeleteRows = false;
            this.dataGridView2hami.AllowUserToResizeRows = false;
            this.dataGridView2hami.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2hami.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.name1,
            this.telll,
            this.tozihatt,
            this.addresss});
            this.dataGridView2hami.Location = new System.Drawing.Point(12, 83);
            this.dataGridView2hami.Name = "dataGridView2hami";
            this.dataGridView2hami.ReadOnly = true;
            this.dataGridView2hami.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dataGridView2hami.RowHeadersVisible = false;
            this.dataGridView2hami.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2hami.Size = new System.Drawing.Size(484, 56);
            this.dataGridView2hami.TabIndex = 19;
            this.dataGridView2hami.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2person_CellContentClick);
            // 
            // name1
            // 
            this.name1.DataPropertyName = "name";
            this.name1.HeaderText = "نام و نام خانوادگی";
            this.name1.Name = "name1";
            this.name1.ReadOnly = true;
            this.name1.Width = 180;
            // 
            // telll
            // 
            this.telll.DataPropertyName = "tell";
            this.telll.HeaderText = "شماره تماس";
            this.telll.Name = "telll";
            this.telll.ReadOnly = true;
            // 
            // tozihatt
            // 
            this.tozihatt.DataPropertyName = "tozihat";
            this.tozihatt.HeaderText = "توضیحات";
            this.tozihatt.Name = "tozihatt";
            this.tozihatt.ReadOnly = true;
            // 
            // addresss
            // 
            this.addresss.DataPropertyName = "address";
            this.addresss.HeaderText = "آدرس";
            this.addresss.Name = "addresss";
            this.addresss.ReadOnly = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(87, 28);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(189, 20);
            this.textBox1.TabIndex = 20;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(307, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "نام مورد نظر را وارد کنید";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(259, 170);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 22;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(87, 170);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 21);
            this.comboBox2.TabIndex = 23;
            // 
            // QueryHami
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 358);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.dataGridView2hami);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "QueryHami";
            this.Text = "QueryHami";
            this.Load += new System.EventHandler(this.QueryHami_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2hami)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView2hami;
        private System.Windows.Forms.DataGridViewTextBoxColumn name1;
        private System.Windows.Forms.DataGridViewTextBoxColumn telll;
        private System.Windows.Forms.DataGridViewTextBoxColumn tozihatt;
        private System.Windows.Forms.DataGridViewTextBoxColumn addresss;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
    }
}