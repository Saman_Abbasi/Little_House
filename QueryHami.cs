﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Little_House__
{
    public partial class QueryHami : DevComponents.DotNetBar.Metro.MetroForm
    {
        LittleEntity Queryy = new LittleEntity();
        public QueryHami()
        {
            InitializeComponent();
        }

        private void hamibutton1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView2person_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void QueryHami_Load(object sender, EventArgs e)
        {
            //comboBox1.DataSource = Queryy.FullHamiProc().ToList();
            //comboBox2.DataSource = Queryy.FullPersonProc().ToList();
            var h = from b in Queryy.Hamis
                    select new { name = b.name + " " + b.lname, b.tell, b.address, b.tozihat };
            dataGridView2hami.DataSource = h.ToList();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox1.Text == null)
            {
                dataGridView2hami.DataSource = Queryy.Hamis.ToList();
            }
            else
            {
                var hamilinq = Queryy.Hamis.ToList();
                var q = from a in hamilinq
                        where a.name.Contains(textBox1.Text) || a.lname.Contains(textBox1.Text) || a.tell.Contains(textBox1.Text)
                        select new { name = a.name + " " + a.lname, a.tell, a.address, a.tozihat };
                dataGridView2hami.DataSource = q.ToList();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
