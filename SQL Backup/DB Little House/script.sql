USE [master]
GO
/****** Object:  Database [little-house]    Script Date: 5/18/2017 8:12:20 PM ******/
CREATE DATABASE [little-house]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'little-house', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\little-house.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'little-house_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\little-house_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [little-house] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [little-house].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [little-house] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [little-house] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [little-house] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [little-house] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [little-house] SET ARITHABORT OFF 
GO
ALTER DATABASE [little-house] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [little-house] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [little-house] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [little-house] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [little-house] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [little-house] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [little-house] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [little-house] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [little-house] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [little-house] SET  DISABLE_BROKER 
GO
ALTER DATABASE [little-house] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [little-house] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [little-house] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [little-house] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [little-house] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [little-house] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [little-house] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [little-house] SET RECOVERY FULL 
GO
ALTER DATABASE [little-house] SET  MULTI_USER 
GO
ALTER DATABASE [little-house] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [little-house] SET DB_CHAINING OFF 
GO
ALTER DATABASE [little-house] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [little-house] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [little-house] SET DELAYED_DURABILITY = DISABLED 
GO
USE [little-house]
GO
/****** Object:  Table [dbo].[BNoekomak]    Script Date: 5/18/2017 8:12:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BNoekomak](
	[id] [tinyint] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](50) NULL,
 CONSTRAINT [PK_BNoekomak] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BNoeposhesh]    Script Date: 5/18/2017 8:12:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BNoeposhesh](
	[id] [tinyint] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](50) NULL,
 CONSTRAINT [PK_BNoeposhesh] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BTahol]    Script Date: 5/18/2017 8:12:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BTahol](
	[id] [tinyint] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](50) NULL,
 CONSTRAINT [PK_BTahol] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BVaziyat]    Script Date: 5/18/2017 8:12:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BVaziyat](
	[id] [tinyint] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](50) NULL,
 CONSTRAINT [PK_BVaziyat] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hami]    Script Date: 5/18/2017 8:12:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hami](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[lname] [nvarchar](50) NULL,
	[tell] [nvarchar](50) NULL,
	[address] [nvarchar](max) NULL,
	[tozihat] [nvarchar](max) NULL,
 CONSTRAINT [PK_Hami] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Person]    Script Date: 5/18/2017 8:12:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Person](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[vaziyat] [tinyint] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[lname] [nvarchar](50) NOT NULL,
	[nationalcode] [char](10) NULL,
	[shenasname] [char](10) NULL,
	[bdate] [date] NULL,
	[faname] [nvarchar](50) NULL,
	[tahol_id] [tinyint] NULL,
	[child] [tinyint] NULL,
	[sarparast] [nvarchar](50) NULL,
	[tahsilat] [nvarchar](50) NULL,
	[address] [nvarchar](max) NULL,
	[tell] [nvarchar](50) NULL,
	[shomarehesab] [nvarchar](50) NULL,
	[noekomak_id] [tinyint] NULL,
	[noeposhesh_id] [tinyint] NULL,
	[uni] [nvarchar](50) NULL,
	[reshte] [nvarchar](50) NULL,
	[datevorod] [date] NULL,
	[maghta] [nvarchar](50) NULL,
	[shahr] [nvarchar](50) NULL,
	[madrese] [nvarchar](50) NULL,
	[noemaloliyat] [nvarchar](50) NULL,
	[vasile] [nvarchar](50) NULL,
	[ax] [varbinary](max) NULL,
	[tozihat] [nvarchar](max) NULL,
 CONSTRAINT [PK_Person] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tarakonesh]    Script Date: 5/18/2017 8:12:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tarakonesh](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[hami_id] [int] NOT NULL,
	[person_id] [int] NOT NULL,
	[datetime] [datetime2](7) NOT NULL,
	[mablagh] [money] NOT NULL,
	[sharh] [nvarchar](max) NULL,
	[tozihat] [nvarchar](max) NULL,
 CONSTRAINT [PK_Tarakonesh_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  View [dbo].[HamiAfrad]    Script Date: 5/18/2017 8:12:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--delete from hami where id=2
--select * from hami
create view [dbo].[HamiAfrad] as select name,lname from hami
GO
ALTER TABLE [dbo].[Person]  WITH CHECK ADD  CONSTRAINT [FK_Person_BNoekomak] FOREIGN KEY([noekomak_id])
REFERENCES [dbo].[BNoekomak] ([id])
GO
ALTER TABLE [dbo].[Person] CHECK CONSTRAINT [FK_Person_BNoekomak]
GO
ALTER TABLE [dbo].[Person]  WITH CHECK ADD  CONSTRAINT [FK_Person_BNoeposhesh] FOREIGN KEY([noeposhesh_id])
REFERENCES [dbo].[BNoeposhesh] ([id])
GO
ALTER TABLE [dbo].[Person] CHECK CONSTRAINT [FK_Person_BNoeposhesh]
GO
ALTER TABLE [dbo].[Person]  WITH CHECK ADD  CONSTRAINT [FK_Person_BTahol1] FOREIGN KEY([tahol_id])
REFERENCES [dbo].[BTahol] ([id])
GO
ALTER TABLE [dbo].[Person] CHECK CONSTRAINT [FK_Person_BTahol1]
GO
ALTER TABLE [dbo].[Person]  WITH CHECK ADD  CONSTRAINT [FK_Person_BVaziyat] FOREIGN KEY([vaziyat])
REFERENCES [dbo].[BVaziyat] ([id])
GO
ALTER TABLE [dbo].[Person] CHECK CONSTRAINT [FK_Person_BVaziyat]
GO
ALTER TABLE [dbo].[Tarakonesh]  WITH CHECK ADD  CONSTRAINT [FK_Tarakonesh_Hami] FOREIGN KEY([hami_id])
REFERENCES [dbo].[Hami] ([id])
GO
ALTER TABLE [dbo].[Tarakonesh] CHECK CONSTRAINT [FK_Tarakonesh_Hami]
GO
ALTER TABLE [dbo].[Tarakonesh]  WITH CHECK ADD  CONSTRAINT [FK_Tarakonesh_Person] FOREIGN KEY([person_id])
REFERENCES [dbo].[Person] ([id])
GO
ALTER TABLE [dbo].[Tarakonesh] CHECK CONSTRAINT [FK_Tarakonesh_Person]
GO
/****** Object:  StoredProcedure [dbo].[FullHamiProc]    Script Date: 5/18/2017 8:12:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[FullHamiProc]
as begin 
select name+' ' +lname as FullHami
from Hami
end
GO
/****** Object:  StoredProcedure [dbo].[FullPersonProc]    Script Date: 5/18/2017 8:12:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[FullPersonProc]
as begin 
select name+' ' +lname as FullPerson
from Person
end
GO
USE [master]
GO
ALTER DATABASE [little-house] SET  READ_WRITE 
GO
