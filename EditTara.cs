﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Little_House__
{
    public partial class EditTara : DevComponents.DotNetBar.Metro.MetroForm
    {
        LittleEntity ComHam = new LittleEntity();
        LittleEntity ComPer = new LittleEntity();
        LittleEntity TarView = new LittleEntity();
        decimal number;
        int idid;

        public EditTara()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (decimal.TryParse(textBox1.Text, out number))
            {
                textBox1.Text = string.Format("{0:n0}", number);
                textBox1.SelectionStart = textBox1.Text.Length;

            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }
        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!((char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Space) || (e.KeyChar == (char)Keys.Back)))
            {
                e.Handled = true;
            }
        }
        private void EditTara_Load(object sender, EventArgs e)
        {
            textBox1.ReadOnly = true; textBox2.ReadOnly = true; textBox3.ReadOnly = true; textBox4.ReadOnly = true; textBox5.ReadOnly = true; textBox6.ReadOnly = true; Sabtbutton.Visible = false; Editbutton.Visible = true;

            idid = Convert.ToInt32(textBox3.Text);
            //comboBox3.DataSource = TarView.FullHamiProc().ToList();

            //comboBox4.DataSource = TarView.FullPersonProc().ToList();
            //====================== Combo Hami ======================//
            //var CH = ComHam.Tarakoneshes.ToList();
            //var CHL = from a in CH.ToList()
            //          where a.id.Equals(Convert.ToInt32(idid))
            //          select new { name = a.Hami.name + " " + a.Hami.lname };
            //comboBox3.DataSource = CHL.ToList();
            //comboBox3.DisplayMember = "name";
            //comboBox3.ValueMember = "id";
            //====================== Combo Person ======================//
            //var CP = ComPer.Tarakoneshes.ToList();
            //var CPL = from b in CP.ToList()
            //          where b.id.Equals(Convert.ToInt32(idid))
            //          select new { name = b.Person.name + " " + b.Person.lname };
            //comboBox4.DataSource = CPL.ToList();
            //comboBox4.DisplayMember = "name";
            //comboBox4.ValueMember = "id";
            //=========================================================//

            var TvL = TarView.Tarakoneshes.ToList();
            var TV = from z in TvL.ToList()
                     where z.id == Convert.ToInt32(idid)
                     select new { z.id, z.mablagh, PName = z.Person.name + " " + z.Person.lname, Hname = z.Hami.name + " " + z.Hami.lname, z.tozihat, z.sharh, dayy = z.datetime.Day, monthh = z.datetime.Month, yearr = z.datetime.Year };
            dataGridViewX2.DataSource = TV.ToList();
            textBox1.Text = dataGridViewX2.CurrentRow.Cells["mablagh"].Value.ToString();
            textBox2.Text = dataGridViewX2.CurrentRow.Cells["sharh"].Value.ToString();
            textBox3.Text = dataGridViewX2.CurrentRow.Cells["tozihat"].Value.ToString();
            textBox6.Text = dataGridViewX2.CurrentRow.Cells["dayy"].Value.ToString();
            textBox5.Text = dataGridViewX2.CurrentRow.Cells["monthh"].Value.ToString();
            textBox4.Text = dataGridViewX2.CurrentRow.Cells["yearr"].Value.ToString();
            comboBox3.DataSource = ComHam.FullHamiProc().Where(a => a.Equals(dataGridViewX2.CurrentRow.Cells["Hname"].Value.ToString()));
            comboBox3.DisplayMember = "FullHami";
            comboBox4.DataSource = ComHam.FullPersonProc().Where(a => a.Equals(dataGridViewX2.CurrentRow.Cells["PName"].Value.ToString()));          

            //comboBox3.DataSource = dataGridViewX2.CurrentRow.Cells["PName"].Value.ToString();
            //comboBox3.DisplayMember = "PName";// dataGridViewX2.CurrentRow.Cells["PName"].Value.ToString();
            //comboBox4.DataSource = dataGridViewX2.CurrentRow.Cells["Hname"].Value.ToString();
            //comboBox4.DisplayMember = dataGridViewX2.CurrentRow.Cells["Hname"].Value.ToString();

        }


        private void Sabtbutton_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("آیا تغییرات ذخیره شود ؟؟؟", "احتیاط", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                var na = textBox1.Text;
                var EditHamiSat = TarView.Tarakoneshes.ToList();
                var EHS = from p in EditHamiSat
                          where p.id == Convert.ToInt32(idid)
                          select p;
                foreach (Tarakonesh p in EHS)
                {
                    p.mablagh = Convert.ToDecimal(textBox1.Text);
                    p.sharh = textBox2.Text;
                    p.tozihat = textBox3.Text;
                    p.datetime = Convert.ToDateTime("2016/12/20");// Convert.ToDateTime(textBox4.Text + "/" + textBox5.Text + "/" + textBox6.Text);
                }
                try
                {
                    TarView.SaveChanges();
                    MessageBox.Show("تغییرات اعمال شد", "ثبت مشخصات", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                catch (Exception)
                {

                    MessageBox.Show("!!!اشکال در ارتباط با بانک اطلاعاتی", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Editbutton_Click(object sender, EventArgs e)
        {
            textBox1.ReadOnly = false; textBox2.ReadOnly = false; textBox3.ReadOnly = false; textBox4.ReadOnly = false; textBox5.ReadOnly = false; textBox6.ReadOnly = false;
            Editbutton.Visible = false; comboBox3.Visible = true; comboBox4.Visible = true;Sabtbutton.Visible = true;
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}