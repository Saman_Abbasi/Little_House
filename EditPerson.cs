﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;


namespace Little_House__
{
    public partial class EditPerson : DevComponents.DotNetBar.Metro.MetroForm
    {
        string idid;
        DateTime bd, vd;

        LittleEntity EditPersonQueryy = new LittleEntity();
        PersianCalendar Persian = new PersianCalendar();
        public EditPerson()
        {
            InitializeComponent();
        }

        private void EditPerson_Load(object sender, EventArgs e)
        {
            comboBox6.SelectedText = Persian.GetDayOfMonth(DateTime.Now).ToString("00");
            comboBox8.SelectedText = Persian.GetYear(DateTime.Now).ToString("0000");
            comboBox7.SelectedIndex = Persian.GetMonth(DateTime.Now) - 1;
            ////=====================================================//
            for (int i = 1310; i <= 1420; i++)
            {
                comboBox8.Items.Add(i);
                comboBox12.Items.Add(i);
            }

            //=====================================================//


            LittleEntity PersonCombo = new LittleEntity();
            comboBox3.DataSource = PersonCombo.BNoekomaks.ToList();
            comboBox3.DisplayMember = "title";
            comboBox3.ValueMember = "id";

            comboBox4.DataSource = PersonCombo.BNoeposheshes.ToList();
            comboBox4.DisplayMember = "title";
            comboBox4.ValueMember = "id";

            comboBox5.DataSource = PersonCombo.BTahols.ToList();
            comboBox5.DisplayMember = "title";
            comboBox5.ValueMember = "id";

            comboBox1.DataSource = PersonCombo.BVaziyats.ToList();
            comboBox1.DisplayMember = "Title";
            comboBox1.ValueMember = "id";

            string d = textBox1.Text;
            idid = d;
            var PersonMainLoad = EditPersonQueryy.People.ToList();
            var qPL = from bPL in PersonMainLoad.ToList()
                      where bPL.id.Equals(Convert.ToInt32(d))
                      select new { bPL.name, bPL.faname, bPL.vaziyat, bPL.lname, vaziyatt = bPL.BVaziyat.title, bPL.shahr, bPL.noekomak_id, bPL.noeposhesh_id, bPL.tahol_id, bnoekomak = bPL.BNoekomak.title, bPL.uni, bPL.reshte,datevorod= bPL.datevorod, bPL.madrese, bPL.noemaloliyat, bPL.vasile, bPL.tozihat, bPL.ax, bPL.maghta, noeposhesh = bPL.BNoeposhesh.title, tahol = bPL.BTahol.title, bPL.child, bPL.sarparast, bPL.tahsilat, bPL.nationalcode, bPL.bdate, bPL.shenasname, bPL.shomarehesab, bPL.tell, bPL.id, bPL.address };
            dataGridView2.DataSource = qPL.ToList();
            textBox1.Text = dataGridView2.CurrentRow.Cells["name"].Value.ToString();
            textBox2.Text = dataGridView2.CurrentRow.Cells["lname"].Value.ToString();
            textBox3.Text = dataGridView2.CurrentRow.Cells["faname"].Value.ToString();
            textBox4.Text = dataGridView2.CurrentRow.Cells["shenasname"].Value.ToString();
            textBox5.Text = dataGridView2.CurrentRow.Cells["nationalcode"].Value.ToString();
            textBox13.Text = dataGridView2.CurrentRow.Cells["child"].Value.ToString();
            textBox14.Text = dataGridView2.CurrentRow.Cells["address"].Value.ToString();
            textBox10.Text = dataGridView2.CurrentRow.Cells["tahsilat"].Value.ToString();
            textBox11.Text = dataGridView2.CurrentRow.Cells["tell"].Value.ToString();
            textBox9.Text = dataGridView2.CurrentRow.Cells["sarparast"].Value.ToString();
            textBox12.Text = dataGridView2.CurrentRow.Cells["shomarehesab"].Value.ToString();
            textBox15.Text = dataGridView2.CurrentRow.Cells["tozihat"].Value.ToString();
            //======================== B DATE ==========================================//
            bd = Convert.ToDateTime(dataGridView2.CurrentRow.Cells["bdate"].Value.ToString());
            comboBox6.SelectedIndex = Convert.ToInt32(bd.Day) - 1;
            comboBox7.SelectedIndex = Convert.ToInt32(bd.Month) - 1;
            comboBox8.Text = Convert.ToString(bd.Year);

            vd = Convert.ToDateTime(dataGridView2.CurrentRow.Cells["datevorod"].Value.ToString());
            comboBox11.SelectedIndex = Convert.ToInt32(vd.Month) - 1;
            comboBox12.Text = Convert.ToString(vd.Year);
            //==========================================================================//
            comboBox3.SelectedIndex = Convert.ToInt32(dataGridView2.CurrentRow.Cells["noekomak_id"].Value) - 1;
            comboBox4.SelectedIndex = Convert.ToInt32(dataGridView2.CurrentRow.Cells["noeposhesh_id"].Value) - 1;
            comboBox5.SelectedIndex = Convert.ToInt32(dataGridView2.CurrentRow.Cells["tahol_id"].Value) - 1;
            comboBox1.SelectedIndex = Convert.ToInt32(dataGridView2.CurrentRow.Cells["vaziyat"].Value) - 1;
            int q = (Convert.ToInt32(dataGridView2.CurrentRow.Cells["vaziyat"].Value) - 1);
            switch (q)
            {

                case 1: // Danesh Jooo
                    //===================  malol   =======================//
                    textBox25.Visible = false;
                    textBox23.Visible = false;
                    label28.Visible = false;
                    label29.Visible = false;
                    //=====================================================//
                    //===================  danesh amooz  ==================//
                    textBox20.Visible = false;
                    textBox21.Visible = false;
                    textBox22.Visible = false;
                    label24.Visible = false;
                    label25.Visible = false;
                    label26.Visible = false;
                    //=====================================================//
                    //==================   danesh joo =====================//
                    comboBox11.Visible = true;
                    comboBox12.Visible = true;
                    label19.Visible = true;
                    label20.Visible = true;
                    label21.Visible = true;
                    label22.Visible = true;
                    label27.Visible = true;
                    textBox7.Visible = true;
                    textBox17.Visible = true;
                    textBox16.Visible = true;
                    textBox19.Visible = true;
                    //========================= SET ============================================

                    textBox16.Text = dataGridView2.CurrentRow.Cells["uni"].Value.ToString();
                    textBox7.Text = dataGridView2.CurrentRow.Cells["reshte"].Value.ToString();
                    textBox19.Text = dataGridView2.CurrentRow.Cells["maghta"].Value.ToString();
                    //
                    textBox17.Text = dataGridView1.CurrentRow.Cells["shahr"].Value.ToString();
                    //
                    //==========================================================================
                    //vd = Convert.ToDateTime(dataGridView2.CurrentRow.Cells["datevorod"].Value.ToString());
                    //comboBox11.SelectedIndex = Convert.ToInt32(vd.Month) - 1;
                    //comboBox12.Text = Convert.ToString(vd.Year);
                    //======================================================//
                    break;
                case 2://Danesh Amoz
                    //==================  malol  ============================//
                    textBox25.Visible = false;
                    textBox23.Visible = false;
                    label28.Visible = false;
                    label29.Visible = false;
                    //=====================================================//
                    //==================   danesh amooz ===================//
                    textBox20.Visible = true;
                    textBox21.Visible = true;
                    textBox22.Visible = true;
                    label24.Visible = true;
                    label25.Visible = true;
                    label26.Visible = true;
                    //============================ SET ===================================
                    textBox19.Text = dataGridView1.CurrentRow.Cells["shahr"].Value.ToString();
                    textBox20.Text = dataGridView2.CurrentRow.Cells["maghta"].Value.ToString();
                    textBox21.Text = dataGridView2.CurrentRow.Cells["madrese"].Value.ToString();
                    //====================================================================

                    //=====================================================//
                    //=================  danesh jooo ======================//
                    label19.Visible = false;
                    label20.Visible = false;
                    label21.Visible = false;
                    label22.Visible = false;
                    label27.Visible = false;
                    textBox7.Visible = false;
                    textBox17.Visible = false;
                    textBox16.Visible = false;
                    textBox19.Visible = false;
                    //comboBox10.Visible = false;
                    comboBox11.Visible = false;
                    comboBox12.Visible = false;
                    //=====================================================//
                    break;
                case 3:// Maloool
                    //===================  malol  ======================//
                    textBox25.Visible = true;
                    textBox23.Visible = true;
                    label28.Visible = true;
                    label29.Visible = true;
                    //======================= SET ============================
                    textBox23.Text = dataGridView1.CurrentRow.Cells["noemaloliyat"].Value.ToString();
                    textBox25.Text = dataGridView1.CurrentRow.Cells["vasile"].Value.ToString();
                    //=====================================================//
                    //=================   danesh amooz ====================//
                    textBox20.Visible = false;
                    textBox21.Visible = false;
                    textBox22.Visible = false;
                    label24.Visible = false;
                    label25.Visible = false;
                    label26.Visible = false;
                    //=====================================================//
                    //====================   danesh jo   ==================//
                    label19.Visible = false;
                    label20.Visible = false;
                    label21.Visible = false;
                    label22.Visible = false;
                    label27.Visible = false;
                    textBox7.Visible = false;
                    textBox17.Visible = false;
                    textBox16.Visible = false;
                    textBox19.Visible = false;
                    //comboBox10.Visible = false;
                    comboBox11.Visible = false;
                    comboBox12.Visible = false;
                    break;

                case 0:
                case 5:
                case 4:
                    //=================  malol  ========================//
                    textBox25.Visible = false;
                    textBox23.Visible = false;
                    label28.Visible = false;
                    label29.Visible = false;
                    //=====================================================//
                    //=====================  danesh amooz =================//
                    textBox20.Visible = false;
                    textBox21.Visible = false;
                    textBox22.Visible = false;
                    label24.Visible = false;
                    label25.Visible = false;
                    label26.Visible = false;
                    //=====================================================//
                    //===================  daneshjoo  ========================//
                    label19.Visible = false;
                    label20.Visible = false;
                    label21.Visible = false;
                    label22.Visible = false;
                    label27.Visible = false;
                    textBox7.Visible = false;
                    textBox17.Visible = false;
                    textBox16.Visible = false;
                    textBox19.Visible = false;
                    //comboBox10.Visible = false;
                    comboBox11.Visible = false;
                    comboBox12.Visible = false;
                    break;

            }

            //=======================================================//
            //PersianCalendar persian = new PersianCalendar();
            //comboBox6.SelectedText = persian.GetDayOfMonth(DateTime.Now).ToString("00");
            //comboBox8.SelectedText = persian.GetYear(DateTime.Now).ToString("0000");
            //comboBox7.SelectedIndex = persian.GetMonth(DateTime.Now) - 1;

            //=====================================================//
            //for (int i = 1310; i <= 1420; i++)
            //{
            //    comboBox8.Items.Add(i);
            //    comboBox12.Items.Add(i);
            //}

            ////=====================================================//


            //LittleEntity PersonCombo1 = new LittleEntity();
            //comboBox3.DataSource = PersonCombo1.BNoekomaks.ToList();
            //comboBox3.DisplayMember = "title";
            //comboBox3.ValueMember = "id";

            //comboBox4.DataSource = PersonCombo1.BNoeposheshes.ToList();
            //comboBox4.DisplayMember = "title";
            //comboBox4.ValueMember = "id";

            //comboBox5.DataSource = PersonCombo1.BTahols.ToList();
            //comboBox5.DisplayMember = "title";
            //comboBox5.ValueMember = "id";

            //comboBox1.DataSource = PersonCombo1.BVaziyats.ToList();
            //comboBox1.DisplayMember = "Title";
            //comboBox1.ValueMember = "id";

        }

        private void button3_Click(object sender, EventArgs e)
        {
            //
            //var na = textBox1.Text;
            LittleEntity context = new LittleEntity();
            var EditPersonSat = EditPersonQueryy.People.ToList();

            var result = MessageBox.Show("آیا تغییرات ذخیره شود ؟؟؟", "احتیاط", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                Person PersonAdd = new Person();
                switch (comboBox1.SelectedIndex)
                {
                    case 1: // Danesh Jooo
                        var EPS = from p in EditPersonSat
                                  where p.id == Convert.ToInt32(idid)
                                  select p;
                        foreach (Person p in EPS)
                        {
                            //=================================  Koli ===================================//
                            p.name = textBox1.Text;
                            p.lname = textBox2.Text;
                            p.faname = textBox3.Text;
                            p.shenasname = textBox4.Text;
                            p.nationalcode = textBox5.Text;
                            p.sarparast = textBox9.Text;
                            p.tahsilat = textBox10.Text;
                            p.tell = textBox11.Text;
                            p.shomarehesab = textBox12.Text;
                            //------------------------- B Date  ---------------------------------//
                            string da1 = (comboBox6.SelectedIndex + 1).ToString("00");//
                            string mon1 = (comboBox7.SelectedIndex + 1).ToString("00");//
                            string yea1 = (comboBox8.SelectedIndex + 1310).ToString("0000");

                            try
                            {
                                string dat1 = yea1 + "/" + mon1 + "/" + da1;
                                p.bdate = Convert.ToDateTime(dat1);
                            }
                            catch (Exception)
                            {
                                string datealaki = "1400/12/31";
                                p.bdate = Convert.ToDateTime(datealaki);
                            }
                            //-----------------------------------------------------------------//

                            //------------------------- Child ---------------------------------//
                            if (textBox13.Text == "" || textBox11.Text == null)
                            {
                                p.child = (Byte)0;
                            }
                            else
                            {
                                p.child = Convert.ToByte(textBox13.Text);
                            }
                            //------------------------- ComboS --------------------------//
                            p.vaziyat = Convert.ToByte(comboBox1.SelectedValue);
                            p.noekomak_id = Convert.ToByte(comboBox3.SelectedValue);
                            p.noeposhesh_id = Convert.ToByte(comboBox4.SelectedValue);
                            p.tahol_id = Convert.ToByte(comboBox5.SelectedValue);
                            //-------------------------------------------------------------//
                            p.address = textBox14.Text;
                            p.tozihat = textBox15.Text;
                            //^^^^^^^^^^^^^^^^^^^^^^ Koli ===================================//
                            //------------------------- V Date  ---------------------------------//

                            string vmon = (comboBox11.SelectedIndex + 1).ToString("00");//
                            string vyea = (comboBox12.SelectedIndex + 1310).ToString("0000");

                            try
                            {
                                string vdat = vyea + "/" + vmon + "/" + "01";
                                p.datevorod = Convert.ToDateTime(vdat);
                            }
                            catch (Exception)
                            {
                                string vdat = "1410/12/31";
                                p.datevorod = Convert.ToDateTime(vdat);
                            }
                            //-----------------------------------------------------------------//
                            p.uni = textBox16.Text;
                            p.reshte = textBox7.Text;
                            p.shahr = textBox17.Text;
                            p.maghta = textBox19.Text;
                            //================================= Save ====================================//
                        }
                        try
                        {


                            EditPersonQueryy.SaveChanges();
                            MessageBox.Show("تغییرات اعمال شد", "ثبت مشخصات", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();

                        }
                        catch (Exception)
                        {

                            MessageBox.Show("!!!اشکال در ارتباط با بانک اطلاعاتی", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        break;
                    case 2: // Danesh AmooZz
                        var EPSdan = from p in EditPersonSat
                                     where p.id == Convert.ToInt32(idid)
                                     select p;
                        foreach (Person p in EPSdan)
                        {
                            //=================================  Koli ===================================//
                            p.name = textBox1.Text;
                            p.lname = textBox2.Text;
                            p.faname = textBox3.Text;
                            p.shenasname = textBox4.Text;
                            p.nationalcode = textBox5.Text;
                            p.sarparast = textBox9.Text;
                            p.tahsilat = textBox10.Text;
                            p.tell = textBox11.Text;
                            p.shomarehesab = textBox12.Text;
                            //------------------------- B Date  ---------------------------------//
                            string da2 = (comboBox6.SelectedIndex + 1).ToString("00");//
                            string mon2 = (comboBox7.SelectedIndex + 1).ToString("00");//
                            string yea2 = (comboBox8.SelectedIndex + 1310).ToString("0000");

                            try
                            {
                                string dat2 = yea2 + "/" + mon2 + "/" + da2;
                                p.bdate = Convert.ToDateTime(dat2);
                            }
                            catch (Exception ex)
                            {
                                string datealaki1 = "1400/12/31";
                                p.bdate = Convert.ToDateTime(datealaki1);
                            }
                            //-----------------------------------------------------------------//
                            //------------------------- Child ---------------------------------//
                            if (textBox13.Text == "" || textBox11.Text == null)
                            {
                                p.child = 0;
                            }
                            else
                            {
                                p.child = Convert.ToByte(textBox13.Text);
                            }
                            //------------------------- ComboS --------------------------//
                            p.vaziyat = Convert.ToByte(comboBox1.SelectedValue);
                            p.noekomak_id = Convert.ToByte(comboBox3.SelectedValue);
                            p.noeposhesh_id = Convert.ToByte(comboBox4.SelectedValue);
                            p.tahol_id = Convert.ToByte(comboBox5.SelectedValue);
                            //-------------------------------------------------------------//
                            p.address = textBox14.Text;
                            p.tozihat = textBox15.Text;
                            //^^^^^^^^^^^^^^^^^^^^^^ Koli ===================================//
                            p.shahr = textBox19.Text;
                            p.maghta = textBox20.Text;
                            p.madrese = textBox21.Text;
                            //================================= Save ====================================//
                        }
                        try
                        {


                            EditPersonQueryy.SaveChanges();
                            MessageBox.Show("تغییرات اعمال شد", "ثبت مشخصات", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();

                        }
                        catch (Exception)
                        {

                            MessageBox.Show("!!!اشکال در ارتباط با بانک اطلاعاتی", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        break;
                    case 3: // MalooL
                        var EPSmal = from p in EditPersonSat
                                     where p.id == Convert.ToInt32(idid)
                                     select p;
                        foreach (Person p in EPSmal)
                        {
                            //=================================  Koli ===================================//
                            p.name = textBox1.Text;
                            p.lname = textBox2.Text;
                            p.faname = textBox3.Text;
                            p.shenasname = textBox4.Text;
                            p.nationalcode = textBox5.Text;
                            p.sarparast = textBox9.Text;
                            p.tahsilat = textBox10.Text;
                            p.tell = textBox11.Text;
                            p.shomarehesab = textBox12.Text;
                            //------------------------- B Date  ---------------------------------//
                            string da3 = (comboBox6.SelectedIndex + 1).ToString("00");//
                            string mon3 = (comboBox7.SelectedIndex + 1).ToString("00");//
                            string yea3 = (comboBox8.SelectedIndex + 1310).ToString("0000");

                            try
                            {
                                string dat3 = yea3 + "/" + mon3 + "/" + da3;
                                p.bdate = Convert.ToDateTime(dat3);
                            }
                            catch (Exception ex)
                            {
                                string datealaki2 = "1400/12/31";
                                p.bdate = Convert.ToDateTime(datealaki2);
                            }
                            //-----------------------------------------------------------------//

                            //------------------------- Child ---------------------------------//
                            if (textBox13.Text == "" || textBox11.Text == null)
                            {
                                p.child = 0;
                            }
                            else
                            {
                                p.child = Convert.ToByte(textBox13.Text);
                            }
                            //------------------------- ComboS --------------------------//
                            p.vaziyat = Convert.ToByte(comboBox1.SelectedValue);
                            p.noekomak_id = Convert.ToByte(comboBox3.SelectedValue);
                            p.noeposhesh_id = Convert.ToByte(comboBox4.SelectedValue);
                            p.tahol_id = Convert.ToByte(comboBox5.SelectedValue);
                            //-------------------------------------------------------------//
                            p.address = textBox14.Text;
                            p.tozihat = textBox15.Text;
                            //^^^^^^^^^^^^^^^^^^^^^^ Koli ===================================//
                            p.noemaloliyat = textBox23.Text;
                            p.vasile = textBox25.Text;
                        }
                        //================================= Save ====================================//
                        try
                        {


                            EditPersonQueryy.SaveChanges();
                            MessageBox.Show("تغییرات اعمال شد", "ثبت مشخصات", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();

                        }
                        catch (Exception)
                        {

                            MessageBox.Show("!!!اشکال در ارتباط با بانک اطلاعاتی", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        break;
                    case 0:
                    case 4:
                    case 5:
                        var EPSkol = from p in EditPersonSat
                                     where p.id == Convert.ToInt32(idid)
                                     select p;
                        foreach (Person p in EPSkol)
                        {
                            //=================================  Koli ===================================//
                            p.name = textBox1.Text;
                            p.lname = textBox2.Text;
                            p.faname = textBox3.Text;
                            p.shenasname = textBox4.Text;
                            p.nationalcode = textBox5.Text;
                            p.sarparast = textBox9.Text;
                            p.tahsilat = textBox10.Text;
                            p.tell = textBox11.Text;
                            p.shomarehesab = textBox12.Text;
                            //------------------------- B Date  ---------------------------------//
                            string da = (comboBox6.SelectedIndex + 1).ToString("00");//
                            string mon = (comboBox7.SelectedIndex + 1).ToString("00");//
                            string yea = (comboBox8.SelectedIndex + 1310).ToString("0000");

                            try
                            {
                                string dat = yea + "/" + mon + "/" + da;
                                p.bdate = Convert.ToDateTime(dat);
                            }
                            catch (Exception )
                            {
                                string datealaki3 = "1400/12/31";
                                p.bdate = Convert.ToDateTime(datealaki3);
                            }
                            //-----------------------------------------------------------------//
                            //string dat = "1400/01/01";
                            //PersonAdd.bdate = Convert.ToDateTime(dat);


                            //-----------------------------------------------------------------//

                            //------------------------- Child ---------------------------------//
                            if (textBox13.Text == "" || textBox11.Text == null)
                            {
                                p.child = (byte)0;
                            }
                            else
                            {
                                p.child = Convert.ToByte(textBox13.Text);
                            }
                            //------------------------- ComboS --------------------------//
                            p.vaziyat = Convert.ToByte(comboBox1.SelectedValue);
                            p.noekomak_id = Convert.ToByte(comboBox3.SelectedValue);
                            p.noeposhesh_id = Convert.ToByte(comboBox4.SelectedValue);
                            p.tahol_id = Convert.ToByte(comboBox5.SelectedValue);
                            //-------------------------------------------------------------//
                            p.address = textBox14.Text;
                            p.tozihat = textBox15.Text;
                        }
                        //^^^^^^^^^^^^^^^^^^^^^^ Koli ===================================//
                        //================================= Save ====================================//
                        try
                        {


                            EditPersonQueryy.SaveChanges();
                            MessageBox.Show("تغییرات اعمال شد", "ثبت مشخصات", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();

                        }
                        catch (Exception)
                        {

                            MessageBox.Show("!!!اشکال در ارتباط با بانک اطلاعاتی", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        break;
                    ////==============================================================================//

                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox1.SelectedIndex)
            {

                case 1: // Danesh Jooo
                    //===================  malol   =======================//
                    textBox25.Visible = false;
                    textBox23.Visible = false;
                    label28.Visible = false;
                    label29.Visible = false;
                    //=====================================================//
                    //===================  danesh amooz  ==================//
                    textBox20.Visible = false;
                    textBox21.Visible = false;
                    textBox22.Visible = false;
                    label24.Visible = false;
                    label25.Visible = false;
                    label26.Visible = false;
                    //=====================================================//
                    //==================   danesh joo =====================//
                    //comboBox10.Visible = true;
                    comboBox11.Visible = true;
                    comboBox12.Visible = true;
                    label19.Visible = true;
                    label20.Visible = true;
                    label21.Visible = true;
                    label22.Visible = true;
                    label27.Visible = true;
                    textBox7.Visible = true;
                    textBox17.Visible = true;
                    textBox16.Visible = true;
                    textBox19.Visible = true;
                    //comboBox10.SelectedText = persian.GetDayOfMonth(DateTime.Now).ToString();
                    //comboBox12.SelectedText = Persian.GetYear(DateTime.Now).ToString();
                    //comboBox11.SelectedIndex = Persian.GetMonth(DateTime.Now) - 1;
                    //======================================================//
                    break;
                case 2:
                    //==================  malol  ============================//
                    textBox25.Visible = false;
                    textBox23.Visible = false;
                    label28.Visible = false;
                    label29.Visible = false;
                    //=====================================================//
                    //==================   danesh amooz ===================//
                    textBox20.Visible = true;
                    textBox21.Visible = true;
                    textBox22.Visible = true;
                    label24.Visible = true;
                    label25.Visible = true;
                    label26.Visible = true;
                    //=====================================================//
                    //=================  danesh jooo ======================//
                    label19.Visible = false;
                    label20.Visible = false;
                    label21.Visible = false;
                    label22.Visible = false;
                    label27.Visible = false;
                    textBox7.Visible = false;
                    textBox17.Visible = false;
                    textBox16.Visible = false;
                    textBox19.Visible = false;
                    //comboBox10.Visible = false;
                    comboBox11.Visible = false;
                    comboBox12.Visible = false;
                    //=====================================================//
                    break;
                case 3:
                    //===================  malol  ======================//
                    textBox25.Visible = true;
                    textBox23.Visible = true;
                    label28.Visible = true;
                    label29.Visible = true;
                    //=====================================================//
                    //=================   danesh amooz ====================//
                    textBox20.Visible = false;
                    textBox21.Visible = false;
                    textBox22.Visible = false;
                    label24.Visible = false;
                    label25.Visible = false;
                    label26.Visible = false;
                    //=====================================================//
                    //====================   danesh jo   ==================//
                    label19.Visible = false;
                    label20.Visible = false;
                    label21.Visible = false;
                    label22.Visible = false;
                    label27.Visible = false;
                    textBox7.Visible = false;
                    textBox17.Visible = false;
                    textBox16.Visible = false;
                    textBox19.Visible = false;
                    //comboBox10.Visible = false;
                    comboBox11.Visible = false;
                    comboBox12.Visible = false;
                    break;

                case 0:
                case 5:
                case 4:
                    //=================  malol  ========================//
                    textBox25.Visible = false;
                    textBox23.Visible = false;
                    label28.Visible = false;
                    label29.Visible = false;
                    //=====================================================//
                    //=====================  danesh amooz =================//
                    textBox20.Visible = false;
                    textBox21.Visible = false;
                    textBox22.Visible = false;
                    label24.Visible = false;
                    label25.Visible = false;
                    label26.Visible = false;
                    //=====================================================//
                    //===================  daneshjoo  ========================//
                    label19.Visible = false;
                    label20.Visible = false;
                    label21.Visible = false;
                    label22.Visible = false;
                    label27.Visible = false;
                    textBox7.Visible = false;
                    textBox17.Visible = false;
                    textBox16.Visible = false;
                    textBox19.Visible = false;
                    //comboBox10.Visible = false;
                    comboBox11.Visible = false;
                    comboBox12.Visible = false;
                    break;

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Visible = false;
            button3.Visible = true;
            textBox1.ReadOnly = false; textBox2.ReadOnly = false; textBox3.ReadOnly = false; textBox4.ReadOnly = false; textBox5.ReadOnly = false; textBox7.ReadOnly = false; textBox9.ReadOnly = false;
            textBox10.ReadOnly = false; textBox11.ReadOnly = false; textBox12.ReadOnly = false; textBox13.ReadOnly = false; textBox14.ReadOnly = false; textBox15.ReadOnly = false; textBox16.ReadOnly = false; textBox17.ReadOnly = false; textBox19.ReadOnly = false;
            textBox20.ReadOnly = false; textBox21.ReadOnly = false; textBox22.ReadOnly = false; textBox23.ReadOnly = false; textBox25.ReadOnly = false;

        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!((char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Space) || (e.KeyChar == (char)Keys.Back)))
            {
                e.Handled = true;
            }
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

    }
}