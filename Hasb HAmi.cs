﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Little_House__
{
    public partial class Hasb_HAmi : DevComponents.DotNetBar.Metro.MetroForm
    {
        LittleEntity tarahami = new LittleEntity();
        public Hasb_HAmi()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            var tarahamix = tarahami.Tarakoneshes.ToList();
           

            var q = from a in tarahamix.ToList()
                    where a.Hami.name.Contains(textBox1.Text) || a.Hami.lname.Contains(textBox1.Text)
                    select new { Name = a.Hami.name + " " + a.Hami.lname, namefard = a.Person.name + " " + a.Person.lname, a.mablagh, a.sharh, a.tozihat, a.datetime };
            dataGridView1.DataSource = q.ToList();

        }

        private void Hasb_HAmi_Load(object sender, EventArgs e)
        {
            var w = from b in tarahami.Tarakoneshes.ToList()
                    select new { Name = b.Hami.name + " " + b.Hami.lname, namefard = b.Person.name + " " + b.Person.lname, b.mablagh, b.sharh, b.tozihat, b.datetime };
            dataGridView1.DataSource = w.ToList();
        }
    }
}
